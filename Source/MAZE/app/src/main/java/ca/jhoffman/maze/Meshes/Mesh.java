package ca.jhoffman.maze.Meshes;

/**
 * Created by jhoffman on 2015-03-04.
 */
public abstract class Mesh
{
    public abstract void draw(float[] projectionViewMatrix, int projectionPosition, int modelPosition);

    public abstract void setScale(float scaleX, float scaleY, float scaleZ);

    public abstract void setPosition(float x, float y, float z);
}
