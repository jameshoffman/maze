package ca.jhoffman.maze.GLUtils.FpsCounter;

public interface IFpsCounterListener
{
    public void onFpsUpdate(int fps);
}
