package ca.jhoffman.maze.Model;

import android.graphics.drawable.shapes.RectShape;

import ca.jhoffman.maze.Meshes.Cube;
import ca.jhoffman.maze.Meshes.Square;
import ca.jhoffman.maze.GlResources;

import static android.opengl.GLES20.GL_FLOAT;
import static android.opengl.GLES20.GL_TEXTURE0;
import static android.opengl.GLES20.GL_TEXTURE_2D;
import static android.opengl.GLES20.glActiveTexture;
import static android.opengl.GLES20.glBindTexture;
import static android.opengl.GLES20.glUniform1i;
import static android.opengl.GLES20.glVertexAttribPointer;

/**
 * Created by jhoffman on 2015-03-04.
 */
public class EndFloorCell extends FloorCell
{
    private FloorCell mFloorCell;

    private BoundingRectangle mFinishBoundingBox;

    public BoundingRectangle getFinishBoundingBox() {
        return mFinishBoundingBox;
    }

    public EndFloorCell(int arrayColIndex, int arrayRowIndex, float scaleX, float scaleY, float glWidth, float glHeight, float finishAreaPercent) {
        super(arrayColIndex, arrayRowIndex, scaleX, scaleY, glWidth, glHeight);

        mFloorCell = new FloorCell(arrayColIndex, arrayRowIndex, scaleX, scaleY, glWidth, glHeight);

        mFinalPositionZ = -Math.max(scaleX, scaleY)/2;
        mInitialPositionZ = -Math.max(scaleX, scaleY)*1.5f;
        mPositionZ = mInitialPositionZ;

        float halfWidth = scaleX/2;
        float halfHeight = scaleY /2;

        float centerX = ((colIndex * scaleX))+halfWidth - glWidth/2;
        float centerY = glHeight/2 - (((rowIndex * scaleY)) + halfHeight);

        mSquareMesh.setPosition(centerX, centerY, mInitialPositionZ);

        mFinishBoundingBox = mBoundingBox.copy();
        mFinishBoundingBox.scale(finishAreaPercent);

        //Smaller bounding box than the cell itself
        //float boundingBoxSize = Math.min(halfHeight, halfWidth) * 0.75f;
        //mBoundingBox = new BoundingRectangle(centerY+boundingBoxSize, centerX+boundingBoxSize, centerY-boundingBoxSize, centerX-boundingBoxSize);
    }

    @Override
    public void draw(float[] modelViewProjectionMatrix, int uniform_projection_matrix_location, int modelPosition)
    {
        if(!isAtFinalPositionZ())
            mFloorCell.draw(modelViewProjectionMatrix, uniform_projection_matrix_location, modelPosition);

        glActiveTexture(GL_TEXTURE0);

        glBindTexture(GL_TEXTURE_2D, GlResources.END_FLOOR_TEXTURE_ID);

        glUniform1i(GlResources.TEXTURE_UNIT_LOCATION, 0);

        GlResources.floorVerticesData.position(0);
        glVertexAttribPointer(GlResources.TEXTURE_ATTRIBUTE_POSITION_LOCATION, Square.POSITION_COMPONENT_COUNT, GL_FLOAT, false, Square.STRIDE, GlResources.floorVerticesData);
        // glEnableVertexAttribArray(VerticesResources.ATTRIBUTE_POSITION_LOCATION);

        GlResources.floorVerticesData.position(Cube.POSITION_COMPONENT_COUNT);
        glVertexAttribPointer(GlResources.TEXTURE_COORDINATE_LOCATION, Square.TEXTURE_COORDINATE_COMPONENT_COUNT, GL_FLOAT, false, Square.STRIDE, GlResources.floorVerticesData);
        //glEnableVertexAttribArray(VerticesResources.ATTRIBUTE_COLOR_LOCATION);

        mSquareMesh.draw(modelViewProjectionMatrix, uniform_projection_matrix_location, modelPosition);
    }
}
