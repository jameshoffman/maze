package ca.jhoffman.maze;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.CheckBox;

import ca.jhoffman.maze.Activities.SettingsActivity;

/**
 * Created by jhoffman on 2015-03-23.
 */
public class SettingsPreferences
{
    private static final String PREFERENCES_FILENAME = "ca.jhoffman.preferences.file";

    public static boolean EnableMazeAnimations;
    private static final String KEY_EnableMazeAnimations = "ca.jhoffman.enable.maze.animations.key";

    public static int SolveDelay;
    private static final String KEY_SolveDelay = "ca.jhoffman.solve.delay.key";

    public static int FinishAreaMultiplier;
    private static final String KEY_FinishAreaMultiplier = "ca.jhoffman.finish.area.multiplier.key";

    public static void loadPreferences(Context context)
    {
        SharedPreferences sharedPref = context.getSharedPreferences(PREFERENCES_FILENAME, Context.MODE_PRIVATE);

        EnableMazeAnimations = sharedPref.getBoolean(KEY_EnableMazeAnimations, true);

        SolveDelay = sharedPref.getInt(KEY_SolveDelay, 3);

        FinishAreaMultiplier = sharedPref.getInt(KEY_FinishAreaMultiplier, 50);
    }

    public static void savePreferences(Context context)
    {
        SharedPreferences sharedPref = context.getSharedPreferences(PREFERENCES_FILENAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();

        editor.putBoolean(KEY_EnableMazeAnimations, EnableMazeAnimations);

        editor.putInt(KEY_SolveDelay, SolveDelay);

        editor.putInt(KEY_FinishAreaMultiplier, FinishAreaMultiplier);

        editor.commit();
    }
}
