precision mediump float;

uniform vec4 uniform_color;

varying vec4 varying_color;

void main()
{
    gl_FragColor = varying_color;
}