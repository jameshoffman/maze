package ca.jhoffman.maze.Model;

import ca.jhoffman.maze.Meshes.Mesh;

import static android.opengl.GLES20.GL_TRIANGLES;
import static android.opengl.GLES20.glDrawArrays;
import static android.opengl.GLES20.glUniformMatrix4fv;
import static android.opengl.Matrix.multiplyMM;

/**
 * Created by jhoffman on 2015-03-02.
 */
public abstract class MazeCell
{
    protected int colIndex;
    protected int rowIndex;

    protected float mPositionZ;
    protected float mInitialPositionZ;
    protected float mFinalPositionZ;

    public abstract void draw(float[] modelViewProjectionMatrix, int uniform_projection_matrix_location, int modelPosition);

    public void updatePositionZ(float speed)
    {
        mPositionZ += speed;

        if (mPositionZ >= mFinalPositionZ)
        {
            mPositionZ = mFinalPositionZ;
        }
        else if (mPositionZ <= mInitialPositionZ)
        {
            mPositionZ = mInitialPositionZ;
        }
    }


    public boolean isAtFinalPositionZ()
    {
        return (mPositionZ == mFinalPositionZ);
    }

    public boolean isAtInitialPositionZ()
    {
        return (mPositionZ == mInitialPositionZ);
    }
}
