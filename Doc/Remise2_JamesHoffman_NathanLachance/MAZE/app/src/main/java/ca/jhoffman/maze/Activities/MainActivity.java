package ca.jhoffman.maze.Activities;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import ca.jhoffman.maze.R;
import ca.jhoffman.maze.SettingsPreferences;

public class MainActivity extends Activity
{
    public static String NEW_MAZE_SIZE_EXTRA_NAME = "ca.jhoffman.maze.new.maze.size.extra";

    private Button mSmallButton;
    private Button mMediumButton;
    private Button mLargeButton;

    private Button mSettingsButton;

    private Button mExitButton;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        mSmallButton = (Button)findViewById(R.id.activity_main_button_small);
        mMediumButton = (Button)findViewById(R.id.activity_main_button_medium);
        mLargeButton = (Button)findViewById(R.id.activity_main_button_large);

        mSmallButton.setOnClickListener(menuButtonClickListener);
        mMediumButton.setOnClickListener(menuButtonClickListener);
        mLargeButton.setOnClickListener(menuButtonClickListener);

        mSettingsButton = (Button)findViewById(R.id.activity_main_button_settings);
        mSettingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, SettingsActivity.class));
            }
        });

        mExitButton = (Button)findViewById(R.id.activity_main_button_exit);
        mExitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private View.OnClickListener menuButtonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            Intent i = new Intent(MainActivity.this, MazeActivity.class);

            i.putExtra(NEW_MAZE_SIZE_EXTRA_NAME, Float.parseFloat((String)v.getTag()));
            startActivity(i);
        }
    };

    @Override
    protected void onResume()
    {
        super.onResume();

        SettingsPreferences.loadPreferences(this);
    }
}
