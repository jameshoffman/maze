# MAZE

Projet Android qui exploite OpenGL ES pour afficher un labyrinthe en 3D vu du dessus. Le gyroscope de l'appareil mobile est utilisé pour déplacer une bille qu'on doit diriger pour résoudre le labyrinthe.

Pour un aperçu du rendu et certains détails d'implémentation, voir [ce document](https://bitbucket.org/jameshoffman/maze/raw/91be5a3d02dc3cee84223246ccee22f242c3b78d/Doc/Remise2_JamesHoffman_NathanLachance/Rapport_Phase2_PDF.pdf).