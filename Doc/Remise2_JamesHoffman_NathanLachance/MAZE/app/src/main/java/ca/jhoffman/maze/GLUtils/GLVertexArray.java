package ca.jhoffman.maze.GLUtils;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import static android.opengl.GLES20.GL_FLOAT;
import static android.opengl.GLES20.glEnableVertexAttribArray;
import static android.opengl.GLES20.glVertexAttribPointer;

/**
 * Created by jhoffman on 15-02-18.
 */
public class GLVertexArray
{
    private final FloatBuffer mFloatBuffer;

    public GLVertexArray(float[] vertexData)
    {
        mFloatBuffer = ByteBuffer.allocateDirect(vertexData.length * GLConstants.BYTES_PER_FLOAT)
                                .order(ByteOrder.nativeOrder())
                                .asFloatBuffer()
                                .put(vertexData);
    }

    public void setVertexAttribPointer(int dataOffset, int attributeLocation, int componentCount, int stride)
    {
        mFloatBuffer.position(dataOffset);

        glVertexAttribPointer(attributeLocation, componentCount, GL_FLOAT, false, stride, mFloatBuffer);

        glEnableVertexAttribArray(attributeLocation);

        mFloatBuffer.position(0);
    }
}
