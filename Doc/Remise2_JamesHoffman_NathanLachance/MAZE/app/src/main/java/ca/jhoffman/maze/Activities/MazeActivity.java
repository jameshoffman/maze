package ca.jhoffman.maze.Activities;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.ConfigurationInfo;
import android.graphics.Color;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.TextView;

import ca.jhoffman.maze.MazeGLSurface;
import ca.jhoffman.maze.MazeGlRenderer;
import ca.jhoffman.maze.R;


public class MazeActivity extends Activity
{
    private MazeGLSurface mGLSurfaceView;
    private TextView mTextview;

    private AlertDialog mPauseDialog;

    private boolean solvedDialogIsShown = false;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        // Check if the system supports OpenGL ES 2.0.
        final ActivityManager activityManager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        final ConfigurationInfo configurationInfo = activityManager.getDeviceConfigurationInfo();

        if(configurationInfo.reqGlEsVersion >= 0x20000)
        {
            float sizeModifier = getIntent().getFloatExtra(MainActivity.NEW_MAZE_SIZE_EXTRA_NAME, 2);

            FrameLayout frameLayout = new FrameLayout(this);
            frameLayout.setLayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT));

            mGLSurfaceView = new MazeGLSurface(this, this);

            mGLSurfaceView.setEGLContextClientVersion(2);

            mGLSurfaceView.setRenderer(new MazeGlRenderer(this, sizeModifier));

            frameLayout.addView(mGLSurfaceView);
            frameLayout.setKeepScreenOn(true);

            mTextview = new TextView(this);
            mTextview.bringToFront();
            mTextview.setTextSize(40.0f);
            mTextview.setTextColor(Color.BLACK);
            mTextview.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.TOP);

            frameLayout.addView(mTextview);

            setContentView(frameLayout);
        }
        else
        {
            setContentView(R.layout.activity_unsupported_opengles_three);
        }
    }



    @Override
    protected void onResume()
    {
        super.onResume();

        if(mGLSurfaceView != null && mGLSurfaceView.hasRenderer())
        {
            View decorView = getWindow().getDecorView();

            hideNavigationAndStatusBar(decorView);

            decorView.setOnSystemUiVisibilityChangeListener(mOnSystemVisibilityChanged);

            mGLSurfaceView.onResume();
        }
    }

    @Override
    protected void onPause()
    {
        if(mGLSurfaceView != null && mGLSurfaceView.hasRenderer())
        {
            mGLSurfaceView.onPause();

            getWindow().getDecorView().setOnSystemUiVisibilityChangeListener(null);
        }

        super.onPause();
    }

    private void hideNavigationAndStatusBar(View decorView)
    {
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION;

        //Hide status bar
        if (Build.VERSION.SDK_INT < 16)
        {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
        else
        {
            uiOptions |= View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE;
        }

        if(Build.VERSION.SDK_INT >= 19)
        {
            uiOptions |= View.SYSTEM_UI_FLAG_IMMERSIVE;
        }

        decorView.setSystemUiVisibility(uiOptions);
    }


    private int hideDelay;
    private CountDownTimer delayCountDown = new CountDownTimer(1001, 10) //1001 so hideDelay is >= 1
    {
        public void onTick(long millisUntilFinished)
        {
            hideDelay -= 10;
        }

        public void onFinish()
        {
            hideDelay = 1;
        }
    };

    private View.OnSystemUiVisibilityChangeListener mOnSystemVisibilityChanged = new View.OnSystemUiVisibilityChangeListener()
    {
        @Override
        public void onSystemUiVisibilityChange(int visibility)
        {
            if (visibility == 0 && mPauseDialog == null && !solvedDialogIsShown)
            {
                MazeActivity.this.mGLSurfaceView.pauseUpdate();

                AlertDialog.Builder builder = new AlertDialog.Builder(MazeActivity.this);

                builder.setTitle("Paused");

                builder.setPositiveButton("Resume", new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int id)
                    {
                        mPauseDialog.cancel();

                        delayedHideNavAndStatusBar();
                    }
                });

                builder.setNegativeButton("Back to menu", new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int id)
                    {
                        mPauseDialog.cancel();

                        MazeActivity.this.finish();
                    }
                });

                /*
                builder.setNeutralButton("Simulate end", new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int id)
                    {
                        mPauseDialog.cancel();

                        MazeActivity.this.mGLSurfaceView.DemoSolved();
                    }
                });
                */


                builder.setCancelable(true);

                builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog)
                    {
                        mPauseDialog.dismiss();
                        mPauseDialog = null;

                        delayedHideNavAndStatusBar();

                        MazeActivity.this.mGLSurfaceView.resumeUpdate();
                    }
                });

                mPauseDialog = builder.create();

                mPauseDialog.show();


                hideDelay = 1001;
                delayCountDown.cancel();
                delayCountDown.start();
            }
        }
    };

    private void delayedHideNavAndStatusBar()
    {
        //Hack to wait for the bar to appear befor hidding it
        new Handler().postDelayed( new Runnable() {
            @Override
            public void run()
            {
                hideNavigationAndStatusBar(getWindow().getDecorView());
            }
        }, hideDelay);
    }

    @Override
    public void onBackPressed()
    {
        //super.onBackPressed();

        if(mPauseDialog != null)
        {
            hideNavigationAndStatusBar(MazeActivity.this.getWindow().getDecorView());
            mPauseDialog = null;
        }
    }

    public void displayMessage(final String message, final int color) {

        runOnUiThread(new Runnable() {
            @Override
            public void run()
            {
                mTextview.setTextColor(color);
                mTextview.setText(message);
            }
        });
    }

    public void displaySolvedDialog() {

        runOnUiThread(new Runnable() {
            @Override
            public void run()
            {
                solvedDialogIsShown = true;
                AlertDialog.Builder builder = new AlertDialog.Builder(MazeActivity.this);

                builder.setTitle("Congratulation!");
                builder.setMessage("You solved the maze. :)");

                builder.setPositiveButton("Play again", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        solvedDialogIsShown = false;
                        delayedHideNavAndStatusBar();

                        MazeActivity.this.mGLSurfaceView.generateMaze();
                    }
                });

                builder.setNegativeButton("Back to menu", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        solvedDialogIsShown = false;

                        MazeActivity.this.finish();
                    }
                });

                builder.setCancelable(false);

                builder.create().show();

                hideDelay = 1001;
                delayCountDown.cancel();
                delayCountDown.start();
            }
        });
    }

    public void clearMessage() {
        runOnUiThread(new Runnable() {
            @Override
            public void run()
            {
                mTextview.setTextColor(Color.BLACK);
                mTextview.setText("");
            }
        });
    }
}
