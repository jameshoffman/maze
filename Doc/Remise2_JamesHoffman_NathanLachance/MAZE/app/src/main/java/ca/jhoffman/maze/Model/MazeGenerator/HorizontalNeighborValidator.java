package ca.jhoffman.maze.Model.MazeGenerator;

/**
 * Created by jhoffman on 2015-03-04.
 */
public class HorizontalNeighborValidator implements INeighborValidator
{
    private int mValue;
    private int mWidth;

    public HorizontalNeighborValidator(int value, int width)
    {
        this.mValue = value;
        this.mWidth = width;
    }

    public boolean isValid(int cellIndex)
    {
        int horizontalIndex = ArrayRepresentationHelper.getArrayColumn(cellIndex, mWidth);

        return (horizontalIndex + mValue >= 0 && horizontalIndex + mValue < mWidth);
    }
}
