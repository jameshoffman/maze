package ca.jhoffman.maze;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;

import ca.jhoffman.maze.Activities.MazeActivity;

/**
 * Created by James on 2014-11-27.
 */
public class MazeGLSurface extends GLSurfaceView
{
    private MazeGlRenderer mRenderer;
    private boolean mHasRenderer = false;
    private MazeActivity mActivity;

    public MazeGLSurface(Context context, AttributeSet attribs)//Constructor with AttributeSet params to allow GLSurface to be inflated from XML layouts
    {
        super(context, attribs);
    }

    public MazeGLSurface(Context context, MazeActivity mazeActivity)
    {
        super(context);
        mActivity = mazeActivity;
    }

    public void setRenderer(MazeGlRenderer renderer)
    {
        super.setRenderer(renderer);

        mRenderer = renderer;

        mRenderer.setGlSurface(this);

        mHasRenderer = true;
    }

    public boolean hasRenderer(){ return mHasRenderer;}

    public void exampleToGLThread(final boolean backgroundIsBlack)
    {
        this.queueEvent(
                new Runnable()
                {
                    @Override
                    public void run()
                    {
                        //glRenderer.setBackgroundIsBlack(backgroundIsBlack);
                    }
                }
        );

        requestRender();
    }

    @Override
    public void onPause()
    {
        if(mRenderer != null)
            mRenderer.onPause();

        super.onPause();
    }

    @Override
    public void onResume()
    {
        super.onResume();

        if(mRenderer != null)
            mRenderer.onResume();
    }

    public void DemoSolved()
    {
        this.queueEvent(
                new Runnable()
                {
                    @Override
                    public void run()
                    {
                        mRenderer.DemoSolved();
                    }
                }
        );
    }

    public void pauseUpdate() {
        this.queueEvent(
                new Runnable()
                {
                    @Override
                    public void run()
                    {
                        mRenderer.onPause();
                    }
                }
        );
    }

    public void resumeUpdate() {
        this.queueEvent(
                new Runnable()
                {
                    @Override
                    public void run()
                    {
                        mRenderer.onResume();
                    }
                }
        );
    }

    public void displayMessage(String message, int color)
    {
        mActivity.displayMessage(message, color);
    }

    public void displaySolvedDialog()
    {
        mActivity.displaySolvedDialog();
    }

    public void generateMaze()
    {
        this.queueEvent(
                new Runnable()
                {
                    @Override
                    public void run()
                    {
                        mRenderer.generateMaze();
                    }
                }
        );
    }

    public void clearMessage() {
        mActivity.clearMessage();
    }
}
