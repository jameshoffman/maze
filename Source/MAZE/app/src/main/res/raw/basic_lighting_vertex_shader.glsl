uniform mat4 uniform_matrix;
uniform mat4 uniform_model_matrix;
uniform mat4 uniform_rotation_translation_matrix;
uniform vec3 uniform_light_position;

attribute vec4 attribute_position;
attribute vec3 attribute_normal;

attribute vec4 attribute_color;
varying vec4 varying_color;

uniform float uniform_point_size;

void main()
{
    vec3 lightPosition = vec3(uniform_light_position[0], uniform_light_position[1], uniform_light_position[1]+10.0);

    mat4 mvp = (uniform_matrix * uniform_model_matrix);

    // Transform the vertex into eye space.
    vec3 modelViewVertex = vec3(uniform_rotation_translation_matrix * attribute_position);

    vec3 normal = attribute_normal;
    vec3 modelViewNormal = vec3(uniform_rotation_translation_matrix * vec4(normal, 0.0));

    // Will be used for attenuation.
    float distance = length(lightPosition - modelViewVertex)/40.0;

    // Get a lighting direction vector from the light to the vertex.
    vec3 lightVector = normalize(lightPosition - modelViewVertex);

    // Calculate the dot product of the light vector and vertex normal. If the normal and light vector are
    // pointing in the same direction then it will get max illumination.
    float diffuse = max(dot(modelViewNormal, lightVector), 0.1);

    // Attenuate the light based on distance.
    diffuse = diffuse * (1.0 / (1.0 + (0.25 * distance * distance)));

    // Multiply the color by the illumination level. It will be interpolated across the triangle.
    varying_color = attribute_color * diffuse;

    gl_Position = mvp * attribute_position;
}

