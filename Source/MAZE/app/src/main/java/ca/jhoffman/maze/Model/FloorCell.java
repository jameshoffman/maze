package ca.jhoffman.maze.Model;

import ca.jhoffman.maze.GlResources;
import ca.jhoffman.maze.Meshes.Cube;
import ca.jhoffman.maze.Meshes.Square;

import static android.opengl.GLES20.GL_FLOAT;
import static android.opengl.GLES20.GL_TEXTURE0;
import static android.opengl.GLES20.GL_TEXTURE_2D;
import static android.opengl.GLES20.glActiveTexture;
import static android.opengl.GLES20.glBindTexture;
import static android.opengl.GLES20.glUniform1i;
import static android.opengl.GLES20.glVertexAttribPointer;

/**
 * Created by jhoffman on 2015-03-02.
 */
public class FloorCell extends MazeCell
{
    protected Square mSquareMesh;
    protected BoundingRectangle mBoundingBox;

    public BoundingRectangle getBoundingBox()
    {
        return mBoundingBox;
    }
    public void setBoundingBox(BoundingRectangle boundingBox){  mBoundingBox = boundingBox; }

    public FloorCell(int arrayColIndex, int arrayRowIndex, float scaleX, float scaleY, float glWidth, float glHeight) {
        mSquareMesh = new Square();

        float scaleZ = Math.max(scaleX, scaleY);
        mSquareMesh.setScale(scaleX, scaleY, scaleZ);

        colIndex = arrayColIndex;
        rowIndex = arrayRowIndex;

        mFinalPositionZ = 0;
        mInitialPositionZ = -scaleZ / 2;
        mPositionZ = mInitialPositionZ;

        float halfWidth = scaleX / 2;
        float halfHeight = scaleY / 2;

        float centerX = ((colIndex * scaleX))+halfWidth - glWidth/2;
        float centerY = glHeight/2 - (((rowIndex * scaleY)) + halfHeight);

        mSquareMesh.setPosition(centerX, centerY, mInitialPositionZ);

        //Smaller bounding box than the cell itself
        //float boundingBoxSize = Math.min(halfHeight, halfWidth) * 0.75f;

        mBoundingBox = new BoundingRectangle(centerY+halfHeight, centerX+halfWidth, centerY-halfHeight, centerX-halfWidth);

        //TODO scale bounding box
        //float ballSize = (Math.min(scaleX, scaleY) * 0.5f);
        //mBoundingBox.setHeight(ballSize);
        //mBoundingBox.setWidth(ballSize);


        //mSquareMesh.setPosition(((colIndex * scaleX))+scaleX/2 - glWidth/2,
                  //              glHeight/2 - (((rowIndex * scaleY)) + scaleY /2),
                //mInitialPositionZ);//transform for gl coordinates, + scale / 2 so it's not the center at the position but the side
    }
    @Override
    public void draw(float[] modelViewProjectionMatrix, int uniform_projection_matrix_location, int modelPosition)
    {
        glActiveTexture(GL_TEXTURE0);

        glBindTexture(GL_TEXTURE_2D, GlResources.FLOOR_TEXTURE_ID);

        glUniform1i(GlResources.TEXTURE_UNIT_LOCATION, 0);

        GlResources.floorVerticesData.position(0);
        glVertexAttribPointer(GlResources.TEXTURE_ATTRIBUTE_POSITION_LOCATION, Square.POSITION_COMPONENT_COUNT, GL_FLOAT, false, Square.STRIDE, GlResources.floorVerticesData);

        GlResources.floorVerticesData.position(Cube.POSITION_COMPONENT_COUNT);
        glVertexAttribPointer(GlResources.TEXTURE_COORDINATE_LOCATION, Square.TEXTURE_COORDINATE_COMPONENT_COUNT, GL_FLOAT, false, Square.STRIDE, GlResources.floorVerticesData);

        mSquareMesh.draw(modelViewProjectionMatrix, uniform_projection_matrix_location, modelPosition);
    }

    @Override
    public void updatePositionZ(float speed)
    {
        super.updatePositionZ(speed);

        mSquareMesh.setPositionZ(mPositionZ);
    }
}
