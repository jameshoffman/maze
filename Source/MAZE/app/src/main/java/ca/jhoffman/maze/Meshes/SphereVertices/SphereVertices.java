package ca.jhoffman.maze.Meshes.SphereVertices;

/**
 * Created by jhoffman on 2015-04-01.
 */
public class SphereVertices
{
    private SphereVertices(){};

    public static float[] LowPoly(){return SphereVerticesLowPoly.Vertices; }

    public static float[] HighPoly(){ return SphereVerticesHighPoly.Vertices; }
}
