package ca.jhoffman.maze.Meshes;


import ca.jhoffman.maze.GlResources;
import ca.jhoffman.maze.Meshes.SphereVertices.SphereVertices;

import static android.opengl.GLES20.GL_FLOAT;
import static android.opengl.GLES20.GL_TRIANGLES;
import static android.opengl.GLES20.glDrawArrays;
import static android.opengl.GLES20.glUniform3f;
import static android.opengl.GLES20.glUniformMatrix4fv;
import static android.opengl.GLES20.glVertexAttribPointer;
import static android.opengl.Matrix.multiplyMM;
import static android.opengl.Matrix.rotateM;
import static android.opengl.Matrix.scaleM;
import static android.opengl.Matrix.setIdentityM;
import static android.opengl.Matrix.translateM;

/**
 * Created by jhoffman on 2015-03-02.
 */
public class Sphere extends Mesh
{
    public static final int BYTES_PER_FLOAT = 4;

    public static final int POSITION_COMPONENT_COUNT = 3;
    public static final int COLOR_COMPONENT_COUNT = 3;
    public static final int NORMAL_COMPONENT_COUNT = 3;

    public static final int STRIDE = (POSITION_COMPONENT_COUNT + COLOR_COMPONENT_COUNT + NORMAL_COMPONENT_COUNT) * BYTES_PER_FLOAT;

    private float[] mModelMatrix;
    private float[] mRotationTranslationModel;
    private float[] mRotationMatrix;

    private float[] mTempMultiplicationMatrix;//only for operations
    private float[] mTempRotationMatrix;//only for operations

    private  float[] mVertices;

    private float scaleX;
    private float scaleY;
    private float scaleZ;

    private float x;
    private float y;
    private float z;

    private float rotationSpeedX;

    private float angleY;
    //**********************************************************************************************
    // Static getters
    //**********************************************************************************************

    public int getVerticesFloatSize()
    {
        return mVertices.length * BYTES_PER_FLOAT;
    }

    public float[] getVertices() {
        return mVertices;
    }

    public int getVerticesCount() {
        return mVertices.length/(POSITION_COMPONENT_COUNT + COLOR_COMPONENT_COUNT + NORMAL_COMPONENT_COUNT);
    }

    //**********************************************************************************************
    // Constructor
    //**********************************************************************************************
    public Sphere(float diameterScale)
    {
        mModelMatrix = new float[16];
        setIdentityM(mModelMatrix, 0);

        mRotationTranslationModel = new float[16];
        setIdentityM(mRotationTranslationModel, 0);

        mRotationMatrix = new float[16];
        setIdentityM(mRotationMatrix, 0);

        mTempMultiplicationMatrix = new float[16];
        mTempRotationMatrix = new float[16];

        setScale(diameterScale, diameterScale, diameterScale);

        if(diameterScale < 0.13f)//0.13 is an arbitrary value
        {
            mVertices = SphereVertices.LowPoly();
        }
        else
        {
            mVertices = SphereVertices.HighPoly();
        }

        rotationSpeedX = 0f;

        angleY = 0f;
    }

    //**********************************************************************************************
    // Methods
    //**********************************************************************************************
    //draw
    public void draw(float[] projectionViewMatrix, int projectionPosition, int modelPosition)
    {
        GlResources.ballVerticesData.position(0);
        glVertexAttribPointer(GlResources.COLOR_ATTRIBUTE_POSITION_LOCATION, Sphere.POSITION_COMPONENT_COUNT, GL_FLOAT, false, Sphere.STRIDE, GlResources.ballVerticesData);

        GlResources.ballVerticesData.position(Sphere.POSITION_COMPONENT_COUNT);
        glVertexAttribPointer(GlResources.COLOR_ATTRIBUTE_COLOR_LOCATION, Sphere.COLOR_COMPONENT_COUNT, GL_FLOAT, false, Sphere.STRIDE, GlResources.ballVerticesData);

        GlResources.ballVerticesData.position(Sphere.POSITION_COMPONENT_COUNT+Sphere.COLOR_COMPONENT_COUNT);
        glVertexAttribPointer(GlResources.COLOR_LIGHTING_ATTRIBUTE_NORMAL_LOCATION, Sphere.NORMAL_COMPONENT_COUNT, GL_FLOAT, false, Sphere.STRIDE, GlResources.ballVerticesData);

        glUniformMatrix4fv(projectionPosition, 1, false, projectionViewMatrix, 0);

        glUniformMatrix4fv(modelPosition, 1, false, mModelMatrix, 0);

        glUniformMatrix4fv(GlResources.COLOR_LIGHTING_UNIFORM_ROTATION_TRANSLATION_MATRIX_LOCATION, 1, false, mRotationTranslationModel, 0);

        glUniform3f(GlResources.COLOR_LIGHTING_UNIFORM_LIGHT_POSITION_LOCATION, x, y, z);

        glDrawArrays(GL_TRIANGLES, 0, getVerticesCount());
    }

    @Override
    public void setScale(float scaleX, float scaleY, float scaleZ)
    {
        //*0.5f to hack the size so its diameter is 1, not 2 like the model
        this.scaleX = scaleX * 0.5f;
        this.scaleY = scaleY * 0.5f;
        this.scaleZ = scaleZ * 0.5f;

        setModelMatrix();
    }

    @Override
    public void setPosition(float x, float y, float z)
    {
        this.x = x;
        this.y = y;
        this.z = z;

        setModelMatrix();
    }

    public void setPosition(float x, float y, float percentSpeedX, float percentSpeedY, float fpsAdjust)
    {
        if(this.x == x)
            percentSpeedX = 0;

        if(this.y == y)
            percentSpeedY = 0;

        if(this.x == x && this.y == y)
        {
            rotationSpeedX = 0;
        }
        else
        {
            //Angle between 1, 0 and current position x, y
            final float refX = 1;
            final float refY = 0;
            final double refLength = Math.sqrt(refX * refX + refY * refY);

            final float directionX = (x - this.x);
            final float directionY = (y - this.y);
            final double directionLength = Math.sqrt(directionX*directionX + directionY*directionY);

            Float tmpAngleY = (float) Math.acos((directionX * refX + refY * directionY) / (directionLength * refLength));

            if (!tmpAngleY.isNaN())
            {
                float newAngle = (float)Math.toDegrees(tmpAngleY.floatValue());
                tmpAngleY = null;

                if (directionY >= 0) {
                    angleY = newAngle * -1.0f;
                } else {
                    angleY = newAngle;
                }

                angleY = (angleY - 90f) * -1;//*-1 to adjust to opengl rotation direction, -90 so going up is 0 degree
            }

            percentSpeedX = Math.abs(percentSpeedX);
            percentSpeedY = Math.abs(percentSpeedY);

            this.rotationSpeedX = (percentSpeedX * 9.8f + percentSpeedY * 9.8f)* 1.5f* fpsAdjust;

            /*
            Calcul angle entre vecteurs

            A = acos(AB.AC/(||AB||*||AC||))

            AB.AC = ABx*ACx + ABy*ACy + ABz*ACz
                    ||AB|| = sqrt(ABx²+ABy²+ABz²)
            ||AC|| = sqrt(ACx²+ACy²+ACz²)
            */
        }

        this.x = x;
        this.y = y;

        setModelMatrix();
    }

    private void setModelMatrix()
    {
        final double angleRadians = Math.toRadians(angleY);
        final double cosAngle =  Math.cos(angleRadians);
        final double sinAngle =  Math.sin(angleRadians);
        final float rotationAxisX = (float)(cosAngle);
        final float rotationAxisY = (float)(sinAngle);

        //Rotation courant
        setIdentityM(mTempRotationMatrix, 0);
        rotateM(mTempRotationMatrix, 0, rotationSpeedX, rotationAxisX, rotationAxisY, 0);

        //Ajout de la rotation courant a la rotation actuelle de la sphere
        multiplyMM(mTempMultiplicationMatrix, 0, mTempRotationMatrix, 0, mRotationMatrix, 0);
        System.arraycopy(mTempMultiplicationMatrix, 0, mRotationMatrix, 0, 16);

        setIdentityM(mModelMatrix, 0);

        translateM(mModelMatrix, 0, x, y, z);

        multiplyMM(mTempMultiplicationMatrix, 0, mModelMatrix, 0, mRotationMatrix, 0);
        System.arraycopy(mTempMultiplicationMatrix, 0, mModelMatrix, 0, 16);

        System.arraycopy(mModelMatrix, 0, mRotationTranslationModel, 0, 16);

        scaleM(mModelMatrix, 0, scaleX, scaleY, scaleZ);
    }
}
