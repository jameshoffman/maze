package ca.jhoffman.maze.Model;

/**
 * Created by jhoffman on 2015-03-05.
 */
public enum MazeState
{
    Preparing,
    Prepared,
    Animating,
    Playing,
    Solved,
    Waiting,
}
