package ca.jhoffman.maze.Model.MazeGenerator;

/**
 * Created by jhoffman on 2015-03-04.
 */
public class CellType
{
    public static Integer Start(){ return -10;}
    public static Integer End(){ return -20;}
    public static Integer Wall(){ return -5;}
    public static Integer Floor(){ return -1;}
}
