package ca.jhoffman.maze.Model;

/**
 * Created by jhoffman on 2015-03-24.
 */
public class FloorCellCollisionBoxes
{
    public BoundingRectangle horizontalRectangle;
    public BoundingRectangle verticalRectangle;

    public FloorCellCollisionBoxes(BoundingRectangle horizontalRectangle, BoundingRectangle verticalRectangle)
    {
        this.horizontalRectangle = horizontalRectangle;
        this.verticalRectangle = verticalRectangle;
    }

    public boolean contains(float x, float y)
    {
        return horizontalRectangle.contains(x, y) || verticalRectangle.contains(x, y);
    }
}
