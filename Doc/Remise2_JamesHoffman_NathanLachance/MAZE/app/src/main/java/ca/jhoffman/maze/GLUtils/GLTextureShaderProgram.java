package ca.jhoffman.maze.GLUtils;

import android.content.Context;

import static android.opengl.GLES20.GL_TEXTURE0;
import static android.opengl.GLES20.GL_TEXTURE_2D;
import static android.opengl.GLES20.glActiveTexture;
import static android.opengl.GLES20.glBindTexture;
import static android.opengl.GLES20.glGetAttribLocation;
import static android.opengl.GLES20.glGetUniformLocation;
import static android.opengl.GLES20.glUniform1i;

/**
 * Created by jhoffman on 15-02-18.
 */
public class GLTextureShaderProgram extends GLShaderProgram
{
    protected static final String UNIFORM_TEXTURE_UNIT_NAME = "uniform_texture_unit";
    protected static final String ATTRIBUTE_TEXTURE_COORDINATES_NAME = "attribute_texture_coordinates";

    private final int uTextureUnitLocation;

    private final int aTextureCoordinatesLocation;

    public GLTextureShaderProgram(Context context, int vertexShaderResourceId, int fragmentShaderResourceId) throws Exception
    {
        super(context, vertexShaderResourceId, fragmentShaderResourceId);

        uTextureUnitLocation = glGetUniformLocation(mProgram, UNIFORM_TEXTURE_UNIT_NAME);

        aTextureCoordinatesLocation = glGetAttribLocation(mProgram, ATTRIBUTE_TEXTURE_COORDINATES_NAME);
    }

    public int getTextureCoordinatesAttributeLocation() {
        return aTextureCoordinatesLocation;
    }

    public void setUniforms(float[] matrix, int textureId)
    {
        setUniforms(matrix);

        glActiveTexture(GL_TEXTURE0);

        glBindTexture(GL_TEXTURE_2D, textureId);

        glUniform1i(uTextureUnitLocation, 0);
    }
}
