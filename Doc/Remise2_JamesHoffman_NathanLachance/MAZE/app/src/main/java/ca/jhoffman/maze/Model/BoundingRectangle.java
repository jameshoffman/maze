package ca.jhoffman.maze.Model;

import android.util.Log;

/**
 * Created by jhoffman on 2015-03-22.
 */
public class BoundingRectangle
{
    public float top;
    public float right;
    public float bottom;
    public float left;

    public BoundingRectangle(float top, float right, float bottom, float left)
    {
        this.top = top;
        this.right = right;
        this.bottom = bottom;
        this.left = left;
    }

    public boolean contains(float x, float y)
    {
        return (x >= left && x <= right&& y <= top && y >= bottom);
    }

    public void scale(float scale)
    {
        float scaleToApply = (1.0f - scale);

        float newHalfWidth = ((right-left)/ 2.0f)*scaleToApply;
        float newHalfHeight = ((top-bottom)/ 2.0f)*scaleToApply;

        this.top -= newHalfHeight;
        this.right -= newHalfWidth;
        this.bottom += newHalfHeight;
        this.left += newHalfWidth;
    }

    public BoundingRectangle copy()
    {
        return new BoundingRectangle(top, right, bottom, left);
    }
}
