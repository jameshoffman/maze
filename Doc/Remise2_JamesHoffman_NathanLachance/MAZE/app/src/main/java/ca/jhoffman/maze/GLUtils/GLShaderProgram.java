package ca.jhoffman.maze.GLUtils;

import android.content.Context;

import static android.opengl.GLES20.glGetAttribLocation;
import static android.opengl.GLES20.glGetUniformLocation;
import static android.opengl.GLES20.glUniformMatrix4fv;
import static android.opengl.GLES20.glUseProgram;

/**
 * Created by jhoffman on 15-02-18.
 */
public class GLShaderProgram
{
    protected static final String UNIFORM_MATRIX_NAME = "uniform_matrix";
    protected static String ATTRIBUTE_POSITION_NAME = "attribute_position";

    protected final int mProgram;

    protected final int uMatrixLocation;

    protected final int aPositionLocation;

    protected GLShaderProgram(Context context, int vertexShaderResourceId, int fragmentShaderResourceId) throws Exception
    {
        // Compile the shaders and link the program.
        mProgram = GLShaderHelper.buildProgram( vertexShaderResourceId, GLShaderType.Vertex20,
                                                fragmentShaderResourceId, GLShaderType.Fragment20,
                                                context);

        uMatrixLocation = glGetUniformLocation(mProgram, UNIFORM_MATRIX_NAME);

        aPositionLocation = glGetAttribLocation(mProgram, ATTRIBUTE_POSITION_NAME);
    }

    public void useProgram()
    {
        glUseProgram(mProgram);
    }

    public int getPositionAttributeLocation() {
        return aPositionLocation;
    }

    public void setUniforms(float[] matrix)
    {
        glUniformMatrix4fv(uMatrixLocation, 1, false, matrix, 0);
    }
}
