package ca.jhoffman.maze.Meshes;

import static android.opengl.GLES20.GL_TRIANGLES;
import static android.opengl.GLES20.glDrawArrays;
import static android.opengl.GLES20.glUniformMatrix4fv;
import static android.opengl.Matrix.multiplyMM;
import static android.opengl.Matrix.scaleM;
import static android.opengl.Matrix.setIdentityM;
import static android.opengl.Matrix.translateM;

public class PlaceHolderFloorSquare//Big square behind the maze with same texture as floor
{
    public static final int BYTES_PER_FLOAT = 4;

    public static final int POSITION_COMPONENT_COUNT = 3;
    public static final int TEXTURE_COORDINATE_COMPONENT_COUNT = 2;

    public static final int STRIDE = (POSITION_COMPONENT_COUNT + TEXTURE_COORDINATE_COMPONENT_COUNT) * BYTES_PER_FLOAT;

    private static float mVertices[] = {

            0.5f, -0.5f, -0.5f,     2.0f, 1.0f,
            0.5f, 0.5f, -0.5f,      2.0f, 0.0f,
            -0.5f, -0.5f, -0.5f,    0.0f, 1.0f,

            0.5f, 0.5f, -0.5f,      2.0f, 0.0f,
            -0.5f, 0.5f, -0.5f,     0.0f, 0.0f,
            -0.5f, -0.5f, -0.5f,    0.0f, 1.0f
    };

    private float[] mModelMatrix;

    private float scaleX;
    private float scaleY;
    private float scaleZ;

    private float x;
    private float y;
    private float z;

    //**********************************************************************************************
    // Static getters
    //**********************************************************************************************

    public static int getVerticesFloatSize()
    {
        return mVertices.length * BYTES_PER_FLOAT;
    }

    public static float[] getVertices() {
        return mVertices;
    }

    public static int getVerticesCount() {
        return 6;
    }

    //**********************************************************************************************
    // Constructor
    //**********************************************************************************************
    public PlaceHolderFloorSquare()
    {
        mModelMatrix = new float[16];
        setIdentityM(mModelMatrix, 0);
    }

    //**********************************************************************************************
    // Methods
    //**********************************************************************************************

    //draw
    public void draw(float[] projectionViewMatrix, int projectionPosition, int modelPosition)
    {
        glUniformMatrix4fv(projectionPosition, 1, false, projectionViewMatrix, 0);

        glUniformMatrix4fv(modelPosition, 1, false, mModelMatrix, 0);

        glDrawArrays(GL_TRIANGLES, 0, getVerticesCount());
    }

    public void setScale(float scaleX, float scaleY, float scaleZ)
    {
        this.scaleX = scaleX;
        this.scaleY = scaleY;
        this.scaleZ = scaleZ;

        setModelMatrix();
    }

    public void setPosition(float x, float y, float z)
    {
        this.x = x;
        this.y = y;
        this.z = z;

        setModelMatrix();
    }

    private void setModelMatrix()
    {
        setIdentityM(mModelMatrix, 0);

        translateM(mModelMatrix, 0, x, y, z);

        scaleM(mModelMatrix, 0, scaleX, scaleY, scaleZ);
    }

    public void setPositionZ(float z)
    {
        this.z = z;

        setModelMatrix();
    }
}
