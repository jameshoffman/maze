package ca.jhoffman.maze.GLUtils;

import android.content.Context;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import static android.opengl.GLES20.GL_COMPILE_STATUS;
import static android.opengl.GLES20.GL_LINK_STATUS;
import static android.opengl.GLES20.GL_VALIDATE_STATUS;
import static android.opengl.GLES20.glAttachShader;
import static android.opengl.GLES20.glCompileShader;
import static android.opengl.GLES20.glCreateProgram;
import static android.opengl.GLES20.glCreateShader;
import static android.opengl.GLES20.glGetProgramInfoLog;
import static android.opengl.GLES20.glGetProgramiv;
import static android.opengl.GLES20.glGetShaderInfoLog;
import static android.opengl.GLES20.glGetShaderiv;
import static android.opengl.GLES20.glLinkProgram;
import static android.opengl.GLES20.glShaderSource;
import static android.opengl.GLES20.glValidateProgram;

/**
 * Created by James on 2014-11-29.
 */

public class GLShaderHelper
{
    public static boolean DEBUG = true;

    private GLShaderHelper(){}

    public static int buildProgram(int vertexShaderCodeFileResId, GLShaderType vertexShaderType, int fragmentShaderCodeFileResId, GLShaderType fragmentShaderType, Context context) throws Exception
    {
        int vertexShader = GLShaderHelper.loadShader(vertexShaderType, vertexShaderCodeFileResId, context);
        int fragmentShader = GLShaderHelper.loadShader(fragmentShaderType, fragmentShaderCodeFileResId, context);

        return GLShaderHelper.linkShadersToProgram(vertexShader, fragmentShader);
    }

    public static int linkShadersToProgram(int vertexShader, int fragmentShader) throws Exception
    {
        final int program = glCreateProgram();

        if(program == 0)
        {
            throw new Exception("Couldn't create program!");
        }

        glAttachShader(program, vertexShader);
        glAttachShader(program, fragmentShader);

        glLinkProgram(program);

        final int[] linkStatus = new int[1];
        glGetProgramiv(program, GL_LINK_STATUS, linkStatus, 0);

        if(linkStatus[0] == 0)
        {
            throw new Exception("Program link failed :\n"+glGetProgramInfoLog(program));
        }

        if(DEBUG)
        {
            glValidateProgram(program);

            final int[] programValidity = new int[1];
            glGetProgramiv(program, GL_VALIDATE_STATUS, programValidity, 0);

            if (programValidity[0] == 0) {
                throw new Exception(glGetProgramInfoLog(program));
            }
        }

        return  program;
    }
    /**
     * Returns a compiled <strong>GL ES 2</strong> shader loaded from the file specified by the parameter "shaderCoderFileResId"
     *
     * @param type Shader type : GLShaderType.Vertex[XX] or GLShaderType.Fragment[XX], where [XX] is the GLES version number
     * @param shaderCodeFileResId
     * @param context
     * @return
     */
    public static int loadShader(GLShaderType type, int shaderCodeFileResId, Context context) throws Exception
    {
        String shaderCode;

        try
        {
            shaderCode = readShaderFile(shaderCodeFileResId, context);
        }
        catch (IOException e)
        {
            throw new Exception(e.getMessage());
        }

        // create a vertex shader type (GLES20.GL_VERTEX_SHADER)
        // or a fragment shader type (GLES20.GL_FRAGMENT_SHADER)
        int shader = glCreateShader(type.getGLShaderType());

        // add the source code to the shader and compile it
        glShaderSource(shader, shaderCode);
        glCompileShader(shader);

        final int[] compileStatus = new int[1];
        glGetShaderiv(shader, GL_COMPILE_STATUS, compileStatus, 0);

        if(compileStatus[0] == 0)//it failed
        {
            throw new Exception("Shader compilation failed :\n"+glGetShaderInfoLog(shader));
        }

        return shader;
    }

    private static String readShaderFile(int shaderCodeFileResId, Context context) throws IOException
    {
        InputStream inputStream = context.getResources().openRawResource(shaderCodeFileResId);

        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);

        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

        StringBuilder sb = new StringBuilder();
        String line = null;
        while ((line = bufferedReader.readLine()) != null)
        {
            sb.append(line).append("\n");
        }
        bufferedReader.close();

        return sb.toString();
    }
}
