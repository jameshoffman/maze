package ca.jhoffman.maze.Model.MazeGenerator;

/**
 * Created by jhoffman on 2015-03-04.
 */
public interface INeighborValidator
{
    boolean isValid(int cellIndex);
}
