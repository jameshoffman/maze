package ca.jhoffman.maze.Model;

import android.graphics.Color;
import android.os.SystemClock;

import java.util.ArrayList;
import java.util.Random;

import ca.jhoffman.maze.GlResources;
import ca.jhoffman.maze.MazeGlRenderer;
import ca.jhoffman.maze.Model.MazeGenerator.ArrayElement;
import ca.jhoffman.maze.Model.MazeGenerator.CellType;
import ca.jhoffman.maze.Model.MazeGenerator.PrimMazeGenerator;
import ca.jhoffman.maze.SettingsPreferences;

import static android.opengl.GLES20.glEnableVertexAttribArray;
import static android.opengl.GLES20.glUseProgram;

/**
 * Created by jhoffman on 2015-03-02.
 */
public class Maze
{
    private MazeGlRenderer mRenderer;

    private PlaceHolderFloor mPlaceHolderFloor;
    private MazeCell[][] mCells;
    private WallCell[] mBorderWalls;
    private MazeCell[] mInnerWalls;
    private EndFloorCell mEndCell;
    private float mInnerWallAnimationSpeed;
    private float mBordersAnimationSpeed;

    private Ball mBall;
    private ArrayList<FloorCellCollisionBoxes> mFloorCollisionBoxes;

    private float mGlWidth;
    private float mGlHeight;

    private float mSizeModifier;
    private int mColsCount;
    private int mRowsCount;

    private float mCellWidth;
    private float mCellHeight;

    private ArrayElement mPrimStart;
    private ArrayElement mPrimEnd;

    private ArrayElement mStart;
    private ArrayElement mEnd;

    private MazeState mState;

    private int mSolveDelay;
    private long mSolvedTimeAccum = -1;
    private long mSolvedStartTime = -1;
    private boolean mSolvedDialogShown = false;

    public MazeState getState() {
        return mState;
    }

    public Integer[][] getMazeCellsType()
    {
        Integer[][] cellsType =  new Integer[mColsCount][mRowsCount];

        for(int i = 0; i < mColsCount; i++)
        {
            for (int j = 0; j < mRowsCount; j++)
            {
                MazeCell cell = mCells[i][j];

                if (cell instanceof WallCell)
                {
                    cellsType[i][j] = CellType.Wall();
                }
                else if (cell instanceof StartFloorCell)
                {
                    cellsType[i][j] = CellType.Start();
                }
                else if (cell instanceof EndFloorCell)
                {
                    cellsType[i][j] = CellType.End();
                }
                else
                {
                    cellsType[i][j] = CellType.Floor();
                }
            }
        }

        return cellsType;
    }

    public Maze(float glWidth, float glHeight, float sizeModifier, MazeGlRenderer renderer)
    {
        mSolveDelay = (SettingsPreferences.SolveDelay * 100000);

        mInnerWallAnimationSpeed = (SettingsPreferences.EnableMazeAnimations ? 0.006f : 1000f);
        mBordersAnimationSpeed = (SettingsPreferences.EnableMazeAnimations ? 0.025f : 1000f);

        mRenderer = renderer;

        mState = MazeState.Preparing;

        mGlWidth = glWidth;
        mGlHeight = glHeight;

        mSizeModifier = sizeModifier;

        initMazeDimensions(mSizeModifier);

        //-3 to get the last row and last col within the border
        mPrimStart = new ArrayElement(0, getRandomValue(0, mRowsCount-3));
        mPrimEnd = new ArrayElement(mColsCount-3, getRandomValue(0, mRowsCount-3));

        Integer[][] cellsType =  PrimMazeGenerator.generate(mPrimStart, mPrimEnd, mColsCount - 2, mRowsCount - 2);//-2 because there is a border around the maze

        mFloorCollisionBoxes = new ArrayList<FloorCellCollisionBoxes>();
        initMaze(cellsType);

        //Call ball constructor after init so mStart and mEnd are initialized
        FloorCellCollisionBoxes[] boundingRectangles = new FloorCellCollisionBoxes[mFloorCollisionBoxes.size()];
        boundingRectangles = mFloorCollisionBoxes.toArray(boundingRectangles);
        mBall = new Ball(mCellWidth, mCellHeight, mGlWidth, mGlHeight, boundingRectangles, mStart, mEnd);

        mPlaceHolderFloor = new PlaceHolderFloor(mCellWidth, mCellHeight, mGlWidth, mGlHeight);
    }

    private int getRandomValue(int min, int max)
    {
        return new Random().nextInt(min + max + 1) + min;
    }

    private void initMazeDimensions(float sizeModifier)
    {
        final float ratio = mGlWidth / mGlHeight;

        //set default maze size
        mColsCount = 16;
        mRowsCount = (int)(mColsCount * ratio);

        mCellWidth = mGlWidth / mColsCount;
        mCellHeight = mGlHeight / mRowsCount;


        float sidesDiff = Math.max(mGlWidth, mGlHeight);

        float modifier = (float)Math.pow(1.5f,sizeModifier);
        //minCols and minRows must be ODD for the Prim algorithm to work
        int minCols = (int)Math.ceil((4.0f+modifier)*ratio);
        if(minCols % 2 == 0)
            minCols++;//is now ODD

        int minRows = (int)Math.ceil(1.0f+modifier);
        if(minRows % 2 == 0)
            minRows++;//is now ODD

        //max is EVEN so we have one more odd value to verify in the loop
        int maxCols = (int)Math.ceil((9.0f+modifier)*(ratio));
        if(maxCols % 2 != 0)
            maxCols++;//is now EVEN

        int maxRows = (int)Math.ceil(8.0f+modifier);
        if(maxRows % 2 != 0)
            maxRows++;//is now EVEN

        //+=2 to have only odd numbers to apply Prim algorithm afterward
        boolean solved  = false;
        for(int i = minCols; i < maxCols; i+=2)
        {
            for(int j = minRows; j < maxRows; j+=2)
            {
                final float tmpCellWidth = mGlWidth / i;
                final float tmpCellHeight = mGlHeight / j;

                final float tmpDiff = Math.abs(tmpCellWidth - tmpCellHeight);

                if(tmpDiff < sidesDiff)
                {
                    sidesDiff = tmpDiff;

                    mColsCount = i;
                    mRowsCount = j;

                    mCellWidth = tmpCellWidth;
                    mCellHeight = tmpCellHeight;

                    if(sidesDiff <= 0.001)
                    {
                        solved = true;
                        break;
                    }
                }
            }

            if(solved)
                break;
        }
    }

    private void initMaze(Integer[][] cellsType)
    {
        mCells = new MazeCell[mColsCount][mRowsCount];

        ArrayList<MazeCell> tmpInnerWalls = new ArrayList<MazeCell>();

        //Initialize maze
        for(int col = 0; col < mColsCount; col++)
        {
            for(int row = 0; row < mRowsCount; row++)
            {
                if(col == 0 || col == (mColsCount - 1) || row == 0 || row == (mRowsCount-1))//it's the border
                {
                    WallCell wall = new WallCell(col, row, mCellWidth, mCellHeight, mGlWidth, mGlHeight);

                    mCells[col][row] = wall;
                }
                else
                {
                    int type = cellsType[col-1][row-1];//-1 to handle the border

                    MazeCell cell;
                    FloorCell floorCellForBoundingBox = null;
                    if (type == CellType.Wall())
                    {
                        cell = new WallCell(col, row, mCellWidth, mCellHeight, mGlWidth, mGlHeight);
                    }
                    else if(type == CellType.Start())
                    {
                        cell = new StartFloorCell(col, row, mCellWidth, mCellHeight, mGlWidth, mGlHeight);

                        floorCellForBoundingBox = (FloorCell)cell;

                        mStart = new ArrayElement(col, row);
                    }
                    else if(type == CellType.End())
                    {
                        mEndCell = new EndFloorCell(col, row, mCellWidth, mCellHeight, mGlWidth, mGlHeight, SettingsPreferences.FinishAreaMultiplier/100.0f);
                        cell = mEndCell;

                        floorCellForBoundingBox = (FloorCell)cell;

                        mEnd = new ArrayElement(col, row);
                    }
                    else
                    {
                        cell = new FloorCell(col, row, mCellWidth, mCellHeight, mGlWidth, mGlHeight);

                        floorCellForBoundingBox = (FloorCell)cell;

                        mCells[col][row] = cell;
                        cell = null;//set to null because it must not be animated with inner walls
                    }

                    if(cell != null)
                    {
                        mCells[col][row] = cell;

                        tmpInnerWalls.add(cell);
                    }

                    if(floorCellForBoundingBox != null)
                    {
                        //cellsType has no border, so -1
                        int i = col - 1;
                        int j = row - 1;

                        //H : horizontal, V : vertical
                        float topH, rightH, bottomH, leftH, topV, rightV, bottomV, leftV;
                        BoundingRectangle floorCellBoundingBox = floorCellForBoundingBox.getBoundingBox();
                        float halfBallDiameter = Math.min(mCellHeight, mCellWidth) * 0.25f;// *0.5 * 0.5 = *0.25

                        //LEFT
                        if((i-1 >= 0 && (cellsType[i-1][j] == CellType.Wall()))  || i-1 < 0)
                        {
                            leftH = leftV = floorCellBoundingBox.left + halfBallDiameter;
                            //leftV = floorCellBoundingBox.left + halfBallDiameter;
                        }
                        else
                        {
                            leftH = floorCellBoundingBox.left;
                            leftV = floorCellBoundingBox.left + halfBallDiameter;
                        }

                        //RIGHT
                        if((i+1 < cellsType.length && (cellsType[i+1][j]  == CellType.Wall())) || i+1 >= cellsType.length)
                        {
                            rightH = rightV = floorCellBoundingBox.right - halfBallDiameter;
                            //rightV = floorCellBoundingBox.right - halfBallDiameter;
                        }
                        else
                        {
                            rightH = floorCellBoundingBox.right;
                            rightV = floorCellBoundingBox.right - halfBallDiameter;
                        }

                        //TOP
                        if((j-1 >= 0 && (cellsType[i][j-1]  == CellType.Wall())) || j-1 < 0)
                        {
                            topH = topV = floorCellBoundingBox.top - halfBallDiameter;
                            //topV = floorCellBoundingBox.top - halfBallDiameter;
                        }
                        else
                        {
                            topH = floorCellBoundingBox.top - halfBallDiameter;
                            topV = floorCellBoundingBox.top;
                        }

                        //BOTTOM
                        if( (j+1 < cellsType[0].length && (cellsType[i][j+1]  == CellType.Wall())) || j+1 >= cellsType[0].length)
                        {
                            bottomH = bottomV = floorCellBoundingBox.bottom + halfBallDiameter;
                            //bottomV = floorCellBoundingBox.bottom + halfBallDiameter;
                        }
                        else
                        {
                            bottomH = floorCellBoundingBox.bottom + halfBallDiameter;
                            bottomV = floorCellBoundingBox.bottom;
                        }


                        mFloorCollisionBoxes.add(
                                                    new FloorCellCollisionBoxes(
                                                                                    new BoundingRectangle(topH, rightH, bottomH, leftH),
                                                                                    new BoundingRectangle(topV, rightV, bottomV, leftV)
                                                                                )
                                                );
                    }
                }
            }
        }

        //Initialize inner walls array
        mInnerWalls = new MazeCell[tmpInnerWalls.size()];
        mInnerWalls = tmpInnerWalls.toArray(mInnerWalls);

        //Initialize borders array
        mBorderWalls = new WallCell[((mColsCount + mRowsCount)*2)-4];

        int middleRow = (int)Math.floor(mRowsCount / 2.0f);

        mBorderWalls[0] = (WallCell)mCells[0][middleRow];
        int borderWallIndex = 1;
        int progressIndex = 1;

        //left
        for(int i = 0; i < middleRow; i++)
        {
            mBorderWalls[borderWallIndex] = (WallCell)mCells[0][middleRow+progressIndex];
            borderWallIndex++;

            mBorderWalls[borderWallIndex] = (WallCell)mCells[0][middleRow-progressIndex];
            borderWallIndex++;

            progressIndex++;
        }

        //top and bottom
        for(int i = 1; i < mColsCount; i++)
        {
            mBorderWalls[borderWallIndex] = (WallCell)mCells[i][0];
            borderWallIndex++;

            mBorderWalls[borderWallIndex] = (WallCell)mCells[i][mRowsCount-1];
            borderWallIndex++;
        }

        //right
        progressIndex = 1;
        for(int i = 0; i < middleRow; i++)
        {
            mBorderWalls[borderWallIndex] = (WallCell)mCells[mColsCount-1][progressIndex];
            borderWallIndex++;

            if(borderWallIndex < mBorderWalls.length)
            {
                mBorderWalls[borderWallIndex] = (WallCell) mCells[mColsCount - 1][mRowsCount - 1 - progressIndex];
                borderWallIndex++;
            }

            progressIndex++;
        }

        mState = MazeState.Prepared;
    }

    public void generateMaze()
    {
        mSolvedDialogShown = false;

        mState = MazeState.Preparing;

        initMazeDimensions(mSizeModifier);

        //-3 to get the last row and last col within the border
        mPrimStart = new ArrayElement(0, getRandomValue(0, mRowsCount-3));
        mPrimEnd = new ArrayElement(mColsCount-3, getRandomValue(0, mRowsCount-3));

        Integer[][] cellsType =  PrimMazeGenerator.generate(mPrimStart, mPrimEnd, mColsCount - 2, mRowsCount - 2);//-2 because there is a border around the maze

        mFloorCollisionBoxes = new ArrayList<FloorCellCollisionBoxes>();
        initMaze(cellsType);

        //Call ball constructor after init so mStart and mEnd are initialized
        FloorCellCollisionBoxes[] boundingRectangles = new FloorCellCollisionBoxes[mFloorCollisionBoxes.size()];
        boundingRectangles = mFloorCollisionBoxes.toArray(boundingRectangles);
        mBall = new Ball(mCellWidth, mCellHeight, mGlWidth, mGlHeight, boundingRectangles, mStart, mEnd);
    }

    public void update(float xPercentSpeed, float yPercentSpeed, float fpsAdjust)
    {
        switch (mState)
        {
            case Prepared:
            {
                appear(fpsAdjust);

                if(mInnerWalls[0].isAtFinalPositionZ())
                {
                    mState = MazeState.Playing;
                }
            }
            break;

            case Playing:
            {
                mBall.update(xPercentSpeed, yPercentSpeed, fpsAdjust);

                if(mBall.reachedEnd(mEndCell.getFinishBoundingBox()))
                {
                    if(mSolvedStartTime == -1)
                    {
                        mSolvedStartTime =  SystemClock.elapsedRealtime();
                    }

                    mSolvedTimeAccum += (SystemClock.elapsedRealtime() - mSolvedStartTime);

                    if(mSolvedTimeAccum >= mSolveDelay)
                    {
                        mState = MazeState.Solved;

                        mRenderer.displayMessage("Solved!", Color.GREEN);
                    }
                    else
                    {
                        mRenderer.displayMessage(""+String.format("%.2f",(float)((mSolveDelay - mSolvedTimeAccum)/1000)/100.f), Color.RED);
                    }
                }
                else
                {
                    mRenderer.clearMessage();

                    mSolvedTimeAccum = -1;
                    mSolvedStartTime = -1;
                }
            }
            break;

            case Solved:
            {
                disappear(fpsAdjust);

                if(mBorderWalls[0].isAtInitialPositionZ())
                {
                    mBorderWallAnimationIndex = 0;
                    mRenderer.clearMessage();

                    if(!mSolvedDialogShown)
                    {
                        mSolvedDialogShown = true;
                        mRenderer.displaySolvedDialog();
                    }
                }
            }
            break;
        }
    }


    public void draw(float[] projectionMatrix)
    {
        if(mState != MazeState.Preparing)
        {
            glUseProgram(GlResources.basicTexturedShaderProgram);

            glEnableVertexAttribArray(GlResources.TEXTURE_ATTRIBUTE_POSITION_LOCATION);
            glEnableVertexAttribArray(GlResources.TEXTURE_COORDINATE_LOCATION);

            for (int i = 0; i < mCells.length; i++)
            {
                for (int j = 0; j < mCells[0].length; j++)
                {
                    mCells[i][j].draw(projectionMatrix, GlResources.TEXTURE_UNIFORM_PROJECTION_MATRIX_LOCATION, GlResources.TEXTURE_UNIFORM_MODEL_MATRIX_LOCATION);
                }
            }
        }

        if(mState == MazeState.Playing)
        {
            glUseProgram(GlResources.basicColoredLightShaderProgram);

            glEnableVertexAttribArray(GlResources.COLOR_ATTRIBUTE_POSITION_LOCATION);
            glEnableVertexAttribArray(GlResources.COLOR_ATTRIBUTE_COLOR_LOCATION);
            glEnableVertexAttribArray(GlResources.COLOR_LIGHTING_ATTRIBUTE_NORMAL_LOCATION);

            mBall.draw(projectionMatrix);
        }
        else
        {
            glUseProgram(GlResources.basicTexturedShaderProgram);

            glEnableVertexAttribArray(GlResources.TEXTURE_ATTRIBUTE_POSITION_LOCATION);
            glEnableVertexAttribArray(GlResources.TEXTURE_COORDINATE_LOCATION);

            mPlaceHolderFloor.draw(projectionMatrix, GlResources.TEXTURE_UNIFORM_PROJECTION_MATRIX_LOCATION);
        }

    }

    private int mBorderWallAnimationIndex = 0;
    public void appear(float fpsAdjust)
    {
        if(!SettingsPreferences.EnableMazeAnimations)
        {
            for(WallCell cell : mBorderWalls)
            {
                cell.updatePositionZ((mBordersAnimationSpeed));
            }

            for(MazeCell cell : mInnerWalls)
            {
                cell.updatePositionZ((mInnerWallAnimationSpeed));
            }

            return;
        }

        if(mBorderWallAnimationIndex < mBorderWalls.length
           && mBorderWalls[mBorderWallAnimationIndex] != null)
        {
            mBorderWalls[mBorderWallAnimationIndex].updatePositionZ((mBordersAnimationSpeed*fpsAdjust));

            if(mBorderWalls[mBorderWallAnimationIndex].isAtFinalPositionZ())
            {
                mBorderWallAnimationIndex++;
            }
        }
        else
        {
            for(int i = 0; i < mInnerWalls.length; i++)
            {
                mInnerWalls[i].updatePositionZ((mInnerWallAnimationSpeed*fpsAdjust));
            }
        }
    }

    public void disappear(float fpsAdjust)
    {
        if(!SettingsPreferences.EnableMazeAnimations)
        {
            for(WallCell cell : mBorderWalls)
            {
                cell.updatePositionZ(-(mBordersAnimationSpeed));
            }

            for(MazeCell cell : mInnerWalls)
            {
                cell.updatePositionZ(-(mInnerWallAnimationSpeed));
            }

            return;
        }



       if(!mInnerWalls[0].isAtInitialPositionZ())
       {
           for(int i = 0; i < mInnerWalls.length; i++)
           {
               mInnerWalls[i].updatePositionZ(-(mInnerWallAnimationSpeed*fpsAdjust));
           }

           mBorderWallAnimationIndex = mBorderWalls.length-1;
       }
       else
       {
           if(mBorderWallAnimationIndex >= 0
              && mBorderWalls[mBorderWallAnimationIndex] != null)
           {
               mBorderWalls[mBorderWallAnimationIndex].updatePositionZ(-(mBordersAnimationSpeed*fpsAdjust));

               if(mBorderWalls[mBorderWallAnimationIndex].isAtInitialPositionZ())
               {
                   mBorderWallAnimationIndex--;
               }
           }
       }
    }

    public void setState(MazeState state) {
        this.mState = state;
    }

    public Ball getBall() {
        return mBall;
    }
}
