package ca.jhoffman.maze.Model;

import ca.jhoffman.maze.GlResources;
import ca.jhoffman.maze.Meshes.Cube;

import static android.opengl.GLES20.GL_FLOAT;
import static android.opengl.GLES20.GL_TEXTURE0;
import static android.opengl.GLES20.GL_TEXTURE_2D;
import static android.opengl.GLES20.glActiveTexture;
import static android.opengl.GLES20.glBindTexture;
import static android.opengl.GLES20.glUniform1i;
import static android.opengl.GLES20.glUniformMatrix4fv;
import static android.opengl.GLES20.glVertexAttribPointer;

/**
 * Created by jhoffman on 2015-03-02.
 */
public class WallCell extends MazeCell
{
    private Cube mCubeMesh;
    private FloorCell mFloorCell;
    private float mScaleZ;

    public WallCell(int arrayColIndex, int arrayRowIndex, float scaleX, float scaleY, float glWidth, float glHeight)
    {
        mCubeMesh = new Cube();

        mFloorCell = new FloorCell(arrayColIndex, arrayRowIndex, scaleX, scaleY, glWidth, glHeight);

        mScaleZ = Math.max(scaleX, scaleY);
        mCubeMesh.setScale(scaleX, scaleY, mScaleZ);

        colIndex = arrayColIndex;
        rowIndex = arrayRowIndex;

        mFinalPositionZ = -mScaleZ/2;
        mInitialPositionZ = -mScaleZ*1.5f;
        mPositionZ = mInitialPositionZ;

        mCubeMesh.setPosition((colIndex * scaleX)+scaleX/2 - glWidth/2,
                            glHeight/2 - ((rowIndex * scaleY) + scaleY /2),
                mInitialPositionZ);//transform for gl coordinates, + scale / 2 so it's not the center at the position but the side
    }

    @Override
    public void draw(float[] modelViewProjectionMatrix, int uniform_projection_matrix_location, int modelPosition)
    {
        if(mPositionZ <= mInitialPositionZ)
            mFloorCell.draw(modelViewProjectionMatrix, uniform_projection_matrix_location, modelPosition);

        glActiveTexture(GL_TEXTURE0);

        glBindTexture(GL_TEXTURE_2D, GlResources.WALLS_TEXTURE_ID);

        glUniform1i(GlResources.TEXTURE_UNIT_LOCATION, 0);

        GlResources.wallVerticesData.position(0);
        glVertexAttribPointer(GlResources.TEXTURE_ATTRIBUTE_POSITION_LOCATION, Cube.POSITION_COMPONENT_COUNT, GL_FLOAT, false, Cube.STRIDE, GlResources.wallVerticesData);

        GlResources.wallVerticesData.position(Cube.POSITION_COMPONENT_COUNT);
        glVertexAttribPointer(GlResources.TEXTURE_COORDINATE_LOCATION, Cube.TEXTURE_COORDINATE_COMPONENT_COUNT, GL_FLOAT, false, Cube.STRIDE, GlResources.wallVerticesData);

        mCubeMesh.draw(modelViewProjectionMatrix, uniform_projection_matrix_location, modelPosition);
    }

    @Override
    public void updatePositionZ(float speed)
    {
        super.updatePositionZ(speed);

        mCubeMesh.setPositionZ(mPositionZ);
    }
}
