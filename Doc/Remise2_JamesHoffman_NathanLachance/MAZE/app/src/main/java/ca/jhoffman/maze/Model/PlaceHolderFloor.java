package ca.jhoffman.maze.Model;

import ca.jhoffman.maze.GlResources;
import ca.jhoffman.maze.Meshes.Cube;
import ca.jhoffman.maze.Meshes.PlaceHolderFloorSquare;

import static android.opengl.GLES20.GL_FLOAT;
import static android.opengl.GLES20.GL_TEXTURE0;
import static android.opengl.GLES20.GL_TEXTURE_2D;
import static android.opengl.GLES20.glActiveTexture;
import static android.opengl.GLES20.glBindTexture;
import static android.opengl.GLES20.glUniform1i;
import static android.opengl.GLES20.glUniformMatrix4fv;
import static android.opengl.GLES20.glVertexAttribPointer;
import static android.opengl.Matrix.scaleM;
import static android.opengl.Matrix.translateM;

/**
 * Created by jhoffman on 2015-03-23.
 */
public class PlaceHolderFloor
{
    private PlaceHolderFloorSquare mPlaceHolderSquare;

    public PlaceHolderFloor(float scaleX, float scaleY, float glWidth, float glHeight)
    {
        mPlaceHolderSquare = new PlaceHolderFloorSquare();

        float scaleZ = Math.max(scaleX, scaleY);

        float mInitialPositionZ = -scaleZ / 2;

        mPlaceHolderSquare.setPosition(0f, 0f, mInitialPositionZ);

        mPlaceHolderSquare.setScale(glWidth*1.2f, glHeight*1.2f, 1.0f);
    }

    public void draw(float[] modelViewProjectionMatrix, int uniform_projection_matrix_location)
    {
        glActiveTexture(GL_TEXTURE0);

        glBindTexture(GL_TEXTURE_2D, GlResources.PLACEHOLDER_FLOOR_TEXTURE_ID);

        glUniform1i(GlResources.TEXTURE_UNIT_LOCATION, 0);

        GlResources.placeHolderFloorVerticesData.position(0);
        glVertexAttribPointer(GlResources.TEXTURE_ATTRIBUTE_POSITION_LOCATION, PlaceHolderFloorSquare.POSITION_COMPONENT_COUNT, GL_FLOAT, false, PlaceHolderFloorSquare.STRIDE, GlResources.placeHolderFloorVerticesData);

        GlResources.floorVerticesData.position(Cube.POSITION_COMPONENT_COUNT);
        glVertexAttribPointer(GlResources.TEXTURE_COORDINATE_LOCATION, PlaceHolderFloorSquare.TEXTURE_COORDINATE_COMPONENT_COUNT, GL_FLOAT, false, PlaceHolderFloorSquare.STRIDE, GlResources.placeHolderFloorVerticesData);

        mPlaceHolderSquare.draw(modelViewProjectionMatrix, uniform_projection_matrix_location, GlResources.TEXTURE_UNIFORM_MODEL_MATRIX_LOCATION);
    }
}

