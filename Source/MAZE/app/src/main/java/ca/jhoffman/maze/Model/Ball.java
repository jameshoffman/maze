package ca.jhoffman.maze.Model;

import android.util.Log;

import org.la4j.Vector;
import org.la4j.vector.dense.BasicVector;

import ca.jhoffman.maze.GlResources;
import ca.jhoffman.maze.Meshes.Sphere;
import ca.jhoffman.maze.Model.MazeGenerator.ArrayElement;

import static android.opengl.GLES20.GL_FLOAT;
import static android.opengl.GLES20.glVertexAttribPointer;

/**
 * Created by jhoffman on 2015-03-22.
 */
public class Ball
{
    private final int X = 0;
    private final int Y = 1;
    private final float MAX_SPEED = 0.035f;

    private Sphere mSphere;

    private Vector mPosition = new BasicVector(2);
    private Vector mSpeed = new BasicVector(2);
    private float diameter;

    private FloorCellCollisionBoxes[] mFloorCellCollisionBoxes;

    public Ball(float cellWidth, float cellHeight, float glWidth, float glHeight, FloorCellCollisionBoxes[] pathBoundingRectangles, ArrayElement start, ArrayElement end)
    {
        diameter = Math.min(cellHeight, cellWidth) * 0.5f;//0.5 so half the width of walls
        mSphere = new Sphere(diameter);

        mPosition.set(X, ((start.Col)*cellWidth) + (cellWidth/2) - (glWidth/2));
        mPosition.set(Y, (glHeight/2) - (((start.Row)*cellHeight)+(cellHeight/2)) );

        float zPosition = Math.max(cellWidth, cellHeight);

        mSphere.setPosition((float)mPosition.get(X),
                            (float)mPosition.get(Y),
                            -(zPosition/2) );

        mFloorCellCollisionBoxes = pathBoundingRectangles;
    }

    public void draw(float[] viewProjectionMatrix)
    {
        mSphere.draw(viewProjectionMatrix, GlResources.COLOR_UNIFORM_PROJECTION_MATRIX_LOCATION, GlResources.COLOR_UNIFORM_MODEL_MATRIX_LOCATION);
    }

    public boolean reachedEnd(BoundingRectangle endBoundingBox)
    {
        return endBoundingBox.contains((float)mPosition.get(X), (float)mPosition.get(Y));
    }

    public void update(float xPercentSpeed, float yPercentSpeed, float fpsAdjust)
    {
        xPercentSpeed *= fpsAdjust;
        yPercentSpeed *= fpsAdjust;

        final float x = (float)mPosition.get(X);
        final float y = (float)mPosition.get(Y);

        //update position
        mSpeed.set(X, xPercentSpeed * MAX_SPEED);
        mSpeed.set(Y, yPercentSpeed * MAX_SPEED);

        mPosition.set(X, x + mSpeed.get(X));
        mPosition.set(Y, y + mSpeed.get(Y));

        final float xMoved = (float)mPosition.get(X);
        final float yMoved = (float)mPosition.get(Y);

        FloorCellCollisionBoxes currentCollisionBoxes = null;
        boolean goesToValidCell = false;
        boolean goesToValidCellUp = false;
        boolean goesToValidCellRight = false;
        boolean goesToValidCellBottom = false;
        boolean goesToValidCellLeft = false;

        for(FloorCellCollisionBoxes cellCollisionBoxes : mFloorCellCollisionBoxes)
        {
            if( cellCollisionBoxes.contains(x, y))
            {
                currentCollisionBoxes = cellCollisionBoxes;
            }

            if(cellCollisionBoxes.contains(xMoved, yMoved))
            {
                goesToValidCell = true;
            }
            else
            {
                if (cellCollisionBoxes.contains(x, yMoved))
                {
                    if(yMoved > y)
                    {
                        goesToValidCellUp = true;
                    }
                    else
                    {
                        goesToValidCellBottom = true;
                    }
                }

                if (cellCollisionBoxes.contains(xMoved, y))
                {
                    if(xMoved > x)
                    {
                        goesToValidCellRight = true;
                    }
                    else
                    {
                        goesToValidCellLeft = true;
                    }
                }
            }

            if(currentCollisionBoxes != null && goesToValidCell)
            {
                break;
            }
        }

        if(goesToValidCell == false && currentCollisionBoxes != null)
        {
            float top, right, bottom, left;

            if( currentCollisionBoxes.horizontalRectangle.contains(x, y)
                    && currentCollisionBoxes.verticalRectangle.contains(x, y))
            {
                top = Math.max(currentCollisionBoxes.horizontalRectangle.top, currentCollisionBoxes.verticalRectangle.top);
                right = Math.max(currentCollisionBoxes.horizontalRectangle.right, currentCollisionBoxes.verticalRectangle.right);
                bottom = Math.min(currentCollisionBoxes.horizontalRectangle.bottom, currentCollisionBoxes.verticalRectangle.bottom);
                left = Math.min(currentCollisionBoxes.horizontalRectangle.left, currentCollisionBoxes.verticalRectangle.left);
            }
            else if( currentCollisionBoxes.horizontalRectangle.contains(x, y))
            {
                top = currentCollisionBoxes.horizontalRectangle.top;
                right = currentCollisionBoxes.horizontalRectangle.right;
                bottom = currentCollisionBoxes.horizontalRectangle.bottom;
                left = currentCollisionBoxes.horizontalRectangle.left;
            }
            else // so currentCollisionBoxes.verticalRectangle.contains(x, y) must be true
            {
                top = currentCollisionBoxes.verticalRectangle.top;
                right = currentCollisionBoxes.verticalRectangle.right;
                bottom = currentCollisionBoxes.verticalRectangle.bottom;
                left = currentCollisionBoxes.verticalRectangle.left;
            }

            if(goesToValidCellUp || goesToValidCellBottom)
            {
                if(xMoved <= left)
                {
                    mPosition.set(X, left);
                }
                else if(xMoved >= right)
                {
                    mPosition.set(X, right);
                }
            }
            else if(goesToValidCellLeft || goesToValidCellRight)
            {
                if(yMoved >= top)
                {
                    mPosition.set(Y, top);
                }
                else if(yMoved <= bottom)
                {
                    mPosition.set(Y, bottom);
                }
            }
            else
            {
                if(yMoved >= top)
                {
                    mPosition.set(Y, top);
                }
                else if(yMoved <= bottom)
                {
                    mPosition.set(Y, bottom);
                }

                if(xMoved <= left)
                {
                    mPosition.set(X, left);
                }
                else if(xMoved >= right)
                {
                    mPosition.set(X, right);
                }
            }
        }

        currentCollisionBoxes = null;
        mSphere.setPosition((float) mPosition.get(X), (float) mPosition.get(Y), xPercentSpeed, yPercentSpeed, fpsAdjust);
    }

    public Sphere getSphere() {
        return mSphere;
    }
}
