package ca.jhoffman.maze.Model.MazeGenerator;

/**
 * Created by jhoffman on 2015-03-04.
 */
public class ArrayRepresentationHelper
{
    public static int getArrayColumn(int continuousCellIndex, int columnCount)
    {
        return continuousCellIndex % columnCount;
    }

    public static int getArrayRow(int continuousCellIndex, int columnCount)
    {
        return continuousCellIndex / columnCount;
    }
}
