package ca.jhoffman.maze.GLUtils;

import android.content.Context;

import static android.opengl.GLES20.glGetAttribLocation;

/**
 * Created by jhoffman on 15-02-18.
 */
public class GLColorShaderProgram extends GLShaderProgram
{
    protected static final String ATTRIBUTE_COLOR_NAME = "attribute_color";

    private final int aColorLocation;

    public GLColorShaderProgram(Context context, int vertexShaderResourceId, int fragmentShaderResourceId) throws Exception
    {
        super(context, vertexShaderResourceId, fragmentShaderResourceId);

        aColorLocation = glGetAttribLocation(mProgram, ATTRIBUTE_COLOR_NAME);
    }

    public int getColorAttributeLocation() {
        return aColorLocation;
    }
}
