package ca.jhoffman.maze;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.opengl.GLSurfaceView;
import android.util.Log;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import ca.jhoffman.maze.GLUtils.FpsCounter.FpsCounter;
import ca.jhoffman.maze.GLUtils.FpsCounter.IFpsCounterListener;
import ca.jhoffman.maze.GLUtils.GLShaderHelper;
import ca.jhoffman.maze.GLUtils.GLShaderType;
import ca.jhoffman.maze.GLUtils.GLTextureHelper;
import ca.jhoffman.maze.Meshes.Cube;
import ca.jhoffman.maze.Meshes.Square;
import ca.jhoffman.maze.Model.Maze;
import ca.jhoffman.maze.Model.MazeState;
import ca.jhoffman.maze.Meshes.PlaceHolderFloorSquare;

import static android.opengl.GLES20.GL_BACK;
import static android.opengl.GLES20.GL_CCW;
import static android.opengl.GLES20.GL_COLOR_BUFFER_BIT;
import static android.opengl.GLES20.GL_CULL_FACE;
import static android.opengl.GLES20.GL_DEPTH_BUFFER_BIT;
import static android.opengl.GLES20.GL_DEPTH_TEST;
import static android.opengl.GLES20.glClear;
import static android.opengl.GLES20.glClearColor;
import static android.opengl.GLES20.glCullFace;
import static android.opengl.GLES20.glEnable;
import static android.opengl.GLES20.glFrontFace;
import static android.opengl.GLES20.glGetAttribLocation;
import static android.opengl.GLES20.glGetUniformLocation;
import static android.opengl.GLES20.glUniformMatrix4fv;
import static android.opengl.GLES20.glUseProgram;
import static android.opengl.GLES20.glVertexAttribPointer;
import static android.opengl.GLES20.glViewport;
import static android.opengl.Matrix.multiplyMM;
import static android.opengl.Matrix.orthoM;
import static android.opengl.Matrix.perspectiveM;
import static android.opengl.Matrix.rotateM;
import static android.opengl.Matrix.scaleM;
import static android.opengl.Matrix.setIdentityM;
import static android.opengl.Matrix.setLookAtM;

/**
 * Created by jhoffman on 15-01-26.
 */
public class MazeGlRenderer implements GLSurfaceView.Renderer, IFpsCounterListener, SensorEventListener
{
    private Context mContext;

    private final float[] mProjectionMatrix = new float[16];
    private final float[] mViewMatrix = new float[16];

    private Maze mMaze;
    private float mSizeModifier;

    private FpsCounter mFpsCounter;
    private MazeGLSurface mGlSurface;

    private boolean mIsPaused;

    private SensorManager mSensorManager;
    private Sensor mAccelerometerSensor;

    private float mFps;

    public MazeGlRenderer(Context context, float sizeModifier)
    {
        this.mContext = context;

        mSizeModifier = sizeModifier;

        mFpsCounter = new FpsCounter(this);
        mFpsCounter.start();

        mIsPaused = false;

        GlResources.wallVerticesData = ByteBuffer.allocateDirect(Cube.getVerticesFloatSize()).order(ByteOrder.nativeOrder()).asFloatBuffer();
        GlResources.wallVerticesData.put(Cube.getVertices());

        GlResources.floorVerticesData = ByteBuffer.allocateDirect(Square.getVerticesFloatSize()).order(ByteOrder.nativeOrder()).asFloatBuffer();
        GlResources.floorVerticesData.put(Square.getFloorVertices());

        GlResources.placeHolderFloorVerticesData = ByteBuffer.allocateDirect(PlaceHolderFloorSquare.getVerticesFloatSize()).order(ByteOrder.nativeOrder()).asFloatBuffer();
        GlResources.placeHolderFloorVerticesData.put(PlaceHolderFloorSquare.getVertices());

        mSensorManager = (SensorManager)context.getSystemService(Context.SENSOR_SERVICE);
        mAccelerometerSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config)
    {
        mSensorManager.registerListener(this, mAccelerometerSensor, SensorManager.SENSOR_DELAY_FASTEST);

        glClearColor(0.5f, 0.5f, 0.5f, 1.0f);

        try
        {
            //Texture shader program
            int texturedVertexShader = GLShaderHelper.loadShader(GLShaderType.Vertex20, R.raw.basic_texture_vertex_shader, mContext);
            int texturedFragmentShader = GLShaderHelper.loadShader(GLShaderType.Fragment20, R.raw.basic_texture_fragment_shader, mContext);

            GlResources.basicTexturedShaderProgram = GLShaderHelper.linkShadersToProgram(texturedVertexShader, texturedFragmentShader);

            //Color Shader program
            int coloredLightVertexShader = GLShaderHelper.loadShader(GLShaderType.Vertex20, R.raw.basic_lighting_vertex_shader, mContext);
            int coloredFragmentShader = GLShaderHelper.loadShader(GLShaderType.Fragment20, R.raw.basic_fragment_shader, mContext);

            GlResources.basicColoredLightShaderProgram = GLShaderHelper.linkShadersToProgram(coloredLightVertexShader, coloredFragmentShader);

            //Textures
            GlResources.FLOOR_TEXTURE_ID = GLTextureHelper.loadTexture(mContext, R.drawable.floor3);
            GlResources.PLACEHOLDER_FLOOR_TEXTURE_ID = GLTextureHelper.loadTexture(mContext, R.drawable.placeholderfloor_half);

            GlResources.START_FLOOR_TEXTURE_ID = GLTextureHelper.loadTexture(mContext, R.drawable.start_floor);
            GlResources.END_FLOOR_TEXTURE_ID = GLTextureHelper.loadTexture(mContext, R.drawable.end_floor);
            GlResources.WALLS_TEXTURE_ID = GLTextureHelper.loadTexture(mContext, R.drawable.walls);
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());

            e.printStackTrace();
        }


        glUseProgram(GlResources.basicColoredLightShaderProgram);
        GlResources.COLOR_UNIFORM_PROJECTION_MATRIX_LOCATION = glGetUniformLocation(GlResources.basicColoredLightShaderProgram, GlResources.UNIFORM_PROJECTION_MATRIX_NAME);
        GlResources.COLOR_ATTRIBUTE_POSITION_LOCATION = glGetAttribLocation(GlResources.basicColoredLightShaderProgram, GlResources.ATTRIBUTE_POSITION_NAME);
        GlResources.COLOR_ATTRIBUTE_COLOR_LOCATION = glGetAttribLocation(GlResources.basicColoredLightShaderProgram, GlResources.ATTRIBUTE_COLOR_NAME);
        GlResources.COLOR_LIGHTING_ATTRIBUTE_NORMAL_LOCATION = glGetAttribLocation(GlResources.basicColoredLightShaderProgram, GlResources.COLOR_LIGHTING_ATTRIBUTE_NORMAL_NAME);
        GlResources.COLOR_UNIFORM_MODEL_MATRIX_LOCATION = glGetUniformLocation(GlResources.basicColoredLightShaderProgram, GlResources.UNIFORM_MODEL_MATRIX_NAME);
        GlResources.COLOR_LIGHTING_UNIFORM_ROTATION_TRANSLATION_MATRIX_LOCATION = glGetUniformLocation(GlResources.basicColoredLightShaderProgram, GlResources.COLOR_LIGHTING_UNIFORM_ROTATION_TRANSLATION_MATRIX_NAME);
        GlResources.COLOR_LIGHTING_UNIFORM_LIGHT_POSITION_LOCATION = glGetUniformLocation(GlResources.basicColoredLightShaderProgram, GlResources.COLOR_LIGHTING_UNIFORM_LIGHT_POSITION_NAME);

        glUseProgram(GlResources.basicTexturedShaderProgram);
        GlResources.TEXTURE_UNIFORM_PROJECTION_MATRIX_LOCATION = glGetUniformLocation(GlResources.basicTexturedShaderProgram, GlResources.UNIFORM_PROJECTION_MATRIX_NAME);
        GlResources.TEXTURE_UNIT_LOCATION = glGetUniformLocation(GlResources.basicTexturedShaderProgram, GlResources.UNIFORM_TEXTURE_UNIT_NAME);
        GlResources.TEXTURE_COORDINATE_LOCATION = glGetAttribLocation(GlResources.basicTexturedShaderProgram, GlResources.ATTRIBUTE_TEXTURE_COORDINATES_NAME);
        GlResources.TEXTURE_ATTRIBUTE_POSITION_LOCATION = glGetAttribLocation(GlResources.basicTexturedShaderProgram, GlResources.ATTRIBUTE_POSITION_NAME);
        GlResources.TEXTURE_UNIFORM_MODEL_MATRIX_LOCATION = glGetUniformLocation(GlResources.basicTexturedShaderProgram, GlResources.UNIFORM_MODEL_MATRIX_NAME);
    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height)
    {
        mMaze = new Maze(((float) width / (float) height)*2.0f, 2.0f, mSizeModifier, this);

        //Load ballVertices here because the ball has been created
        GlResources.ballVerticesData = ByteBuffer.allocateDirect(mMaze.getBall().getSphere().getVerticesFloatSize()).order(ByteOrder.nativeOrder()).asFloatBuffer();
        GlResources.ballVerticesData.put(mMaze.getBall().getSphere().getVertices());

        glViewport(0, 0, width, height);

        glEnable(GL_DEPTH_TEST);

        glFrontFace(GL_CCW);
        glCullFace(GL_BACK);
        glEnable(GL_CULL_FACE);

        calculateProjection(width, height);
    }

    private void calculateProjection(int width, int height)
    {
        final boolean isPerspective = true;

        final boolean isLandscape = (width > height);
        final float aspectRatio = isLandscape || isPerspective ? ((float) width / (float) height) : ((float) height / (float) width);

        final float left = isLandscape ? -aspectRatio :  -1.0f;
        final float right = isLandscape ? aspectRatio : 1.0f;
        final float top = isLandscape ? 1.0f : aspectRatio;
        final float bottom = isLandscape ? -1.0f : -aspectRatio;
        final float near = -1.0f;
        final float far = 1.0f;

        if(isPerspective)
        {
            final float viewDepth = far - near;
            final float viewHeight = top - bottom;
            final float fovDegree = 35f;
            final float fovRadians = (float)Math.toRadians(fovDegree);

            final float viewNear = (float)((viewHeight / 2.0f) / Math.tan(fovRadians / 2.0f));
            final float viewFar = viewNear + viewDepth;

            //aspect is ALWAYS width / height in perspective!!!!!
            perspectiveM(mProjectionMatrix, 0, fovDegree, ((float) width / (float) height), 0.1f, viewFar);

            setLookAtM(mViewMatrix, 0, 0, 0, viewNear, 0f, 0f, 0f, 0f, 1.0f, 0.0f);
        }
        else
        {
            setIdentityM(mViewMatrix, 0);

            orthoM(mProjectionMatrix, 0, left, right, bottom, top, near, far);
        }

        //Update projection according to view matrix
        float[] temp = new float[16];
        multiplyMM(temp, 0, mProjectionMatrix, 0, mViewMatrix, 0);
        System.arraycopy(temp, 0, mProjectionMatrix, 0, temp.length);
    }

    @Override
    public void onDrawFrame(GL10 gl)
    {
        mFpsCounter.onNewFrame();

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        if(!mIsPaused)
            mMaze.update(xPercent, yPercent, 1.0f + (1.0f - (mFps/60.0f)));

        mMaze.draw(mProjectionMatrix);
    }

    @Override
    public void onFpsUpdate(int fps)
    {
        Log.i("FPS", "FPS : " + fps);

        if(fps > 0)
        {
            mFps = Math.min(fps, 60f);
        }
    }

    public void onPause()
    {
        mIsPaused = true;
        mFpsCounter.stop();

        mSensorManager.unregisterListener(this);
    }

    public void onResume()
    {
        mIsPaused = false;
        mFpsCounter.start();

        mSensorManager.registerListener(this, mAccelerometerSensor,  SensorManager.SENSOR_DELAY_FASTEST);
    }

    public void setGlSurface(MazeGLSurface glSurface) {
        this.mGlSurface = glSurface;
    }

    public void DemoSolved() {
        mMaze.setState(MazeState.Solved);
    }

    //ACCELEROMETER
    private float xPercent = 0f;
    private float yPercent = 0f;

    @Override
    public void onSensorChanged(SensorEvent event)
    {
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER)
        {
            xPercent = (event.values[1] * 100f)/980f;

            yPercent = (event.values[0] * -100f)/980f;//-100 to invert direction

            if(xPercent > 100f)
            {
                xPercent = 100f;
            }
            else if(xPercent < -100f)
            {
                xPercent = -100f;
            }

            if(yPercent > 100f)
            {
                yPercent = 100f;
            }
            else if(yPercent < -100f)
            {
                yPercent = -100f;
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    public void displayMessage(String message, int color)
    {
        mGlSurface.displayMessage(message, color);
    }

    public void displaySolvedDialog()
    {
        mGlSurface.displaySolvedDialog();
    }

    public void generateMaze()
    {
        mMaze.generateMaze();
    }

    public void clearMessage()
    {
        mGlSurface.clearMessage();
    }
}
