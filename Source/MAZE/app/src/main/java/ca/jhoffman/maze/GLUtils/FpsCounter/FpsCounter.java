package ca.jhoffman.maze.GLUtils.FpsCounter;

import android.os.CountDownTimer;

/**
 * Created by jhoffman on 2015-03-04.
 */
public class FpsCounter
{
    private int mFps;
    private boolean isPaused;
    private IFpsCounterListener mListener;

    private CountDownTimer fpsTimer = new CountDownTimer(1000, 1000)
    {
        public void onTick(long millisUntilFinished)
        {

        }

        public void onFinish()
        {
            if(mListener != null && !isPaused)
            {
                mListener.onFpsUpdate(mFps);
            }

            mFps = 0;

            fpsTimer.start();
        }
    };

    public FpsCounter(IFpsCounterListener listener)
    {
        mFps = 0;

        isPaused = false;
        
        mListener = listener;
    }

    public void start()
    {
        fpsTimer.start();
        isPaused = false;
    }

    public void onNewFrame()
    {
        mFps++;
    }

    public void stop() {
        fpsTimer.cancel();
        isPaused = true;
    }
}

