uniform mat4 uniform_matrix;
uniform mat4 uniform_model_matrix;

attribute vec4 attribute_position;
attribute vec2 attribute_texture_coordinates;

varying vec2 varying_texture_coordinates;

void main()
{
    varying_texture_coordinates = attribute_texture_coordinates;

    mat4 mvp = (uniform_matrix * uniform_model_matrix);
    gl_Position = mvp * attribute_position;
}