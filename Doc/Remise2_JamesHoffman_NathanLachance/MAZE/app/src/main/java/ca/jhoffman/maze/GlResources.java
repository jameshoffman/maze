package ca.jhoffman.maze;

import java.nio.FloatBuffer;

/**
 * Created by jhoffman on 2015-03-04.
 */
public class GlResources
{
    public static int basicTexturedShaderProgram;
    public static int basicColoredLightShaderProgram;

    public static int COLOR_LIGHTING_ATTRIBUTE_NORMAL_LOCATION;
    public static String COLOR_LIGHTING_ATTRIBUTE_NORMAL_NAME = "attribute_normal";

    public static String COLOR_LIGHTING_UNIFORM_ROTATION_TRANSLATION_MATRIX_NAME = "uniform_rotation_translation_matrix";
    public static int COLOR_LIGHTING_UNIFORM_ROTATION_TRANSLATION_MATRIX_LOCATION;

    public static String COLOR_LIGHTING_UNIFORM_LIGHT_POSITION_NAME = "uniform_light_position";
    public static int COLOR_LIGHTING_UNIFORM_LIGHT_POSITION_LOCATION;

    public static int COLOR_ATTRIBUTE_COLOR_LOCATION;
    public static String ATTRIBUTE_COLOR_NAME = "attribute_color";

    public static String UNIFORM_PROJECTION_MATRIX_NAME = "uniform_matrix";
    public static int COLOR_UNIFORM_PROJECTION_MATRIX_LOCATION;

    public static String ATTRIBUTE_POSITION_NAME = "attribute_position";
    public static int COLOR_ATTRIBUTE_POSITION_LOCATION;

    public static int TEXTURE_UNIFORM_PROJECTION_MATRIX_LOCATION;

    public static String UNIFORM_MODEL_MATRIX_NAME = "uniform_model_matrix";
    public static int COLOR_UNIFORM_MODEL_MATRIX_LOCATION;
    public static int TEXTURE_UNIFORM_MODEL_MATRIX_LOCATION;

    public static int TEXTURE_ATTRIBUTE_POSITION_LOCATION;

    public static FloatBuffer  wallVerticesData;
    public static  FloatBuffer floorVerticesData;
    public static  FloatBuffer ballVerticesData;
    public static  FloatBuffer placeHolderFloorVerticesData;


    public static final String UNIFORM_TEXTURE_UNIT_NAME = "uniform_texture_unit";
    public static int TEXTURE_UNIT_LOCATION;

    public static final String ATTRIBUTE_TEXTURE_COORDINATES_NAME = "attribute_texture_coordinates";
    public static int TEXTURE_COORDINATE_LOCATION;

    public static int FLOOR_TEXTURE_ID;
    public static int START_FLOOR_TEXTURE_ID;
    public static int END_FLOOR_TEXTURE_ID;
    public static int WALLS_TEXTURE_ID;
    public static int PLACEHOLDER_FLOOR_TEXTURE_ID;
}
