package ca.jhoffman.maze.Activities;

import android.app.Activity;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.TextView;

import ca.jhoffman.maze.R;
import ca.jhoffman.maze.SettingsPreferences;

public class SettingsActivity extends Activity
{
    private CheckBox mEnableMazeAnimationsCheckBox;

    private TextView mSolveDelayTitle;
    private SeekBar mSolveDelaySeekbar;

    private TextView mFinishAreaTitle;
    private SeekBar mFinishAreaSeekbar;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        mEnableMazeAnimationsCheckBox = (CheckBox)findViewById(R.id.activity_settings_enable_maze_animations);
        mEnableMazeAnimationsCheckBox.setOnCheckedChangeListener(onEnableMazeAnimationsCheckedChanged);

        mSolveDelayTitle = (TextView)findViewById(R.id.activity_settings_solve_delay_title);
        mSolveDelaySeekbar = (SeekBar)findViewById(R.id.activity_settings_solve_delay_seekbar);
        mSolveDelaySeekbar.setOnSeekBarChangeListener(onSolveDelaySeekbarChanged);

        mFinishAreaTitle = (TextView)findViewById(R.id.activity_settings_finish_area_size_title);
        mFinishAreaSeekbar = (SeekBar)findViewById(R.id.activity_settings_finish_area_size_seekbar);
        mFinishAreaSeekbar.setOnSeekBarChangeListener(onFinishAreaSeekbarChanged);


        ((Button)findViewById(R.id.activity_settings_button_back)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SettingsActivity.this.finish();
            }
        });
    }

    @Override
    protected void onResume()
    {
        super.onResume();

        SettingsPreferences.loadPreferences(this);

        mEnableMazeAnimationsCheckBox.setChecked(SettingsPreferences.EnableMazeAnimations);

        mSolveDelaySeekbar.setProgress(SettingsPreferences.SolveDelay);
        mSolveDelayTitle.setText(String.format("Solve delay (%d sec.)", SettingsPreferences.SolveDelay));

        mFinishAreaSeekbar.setProgress(SettingsPreferences.FinishAreaMultiplier-10);
        mFinishAreaTitle.setText(String.format("Finish area (%d %%)", SettingsPreferences.FinishAreaMultiplier));
    }

    private CompoundButton.OnCheckedChangeListener onEnableMazeAnimationsCheckedChanged = new CompoundButton.OnCheckedChangeListener()
    {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

            SettingsPreferences.EnableMazeAnimations = isChecked;

            SettingsPreferences.savePreferences(SettingsActivity.this);
        }
    };

    private SeekBar.OnSeekBarChangeListener onSolveDelaySeekbarChanged = new SeekBar.OnSeekBarChangeListener()
    {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser)
        {
            mSolveDelayTitle.setText(String.format("Solve delay (%d sec.)", progress));
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar)
        {
            SettingsPreferences.SolveDelay = seekBar.getProgress();

            SettingsPreferences.savePreferences(SettingsActivity.this);
        }
    };

    private SeekBar.OnSeekBarChangeListener onFinishAreaSeekbarChanged = new SeekBar.OnSeekBarChangeListener()
    {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser)
        {
            mFinishAreaTitle.setText(String.format("Solve delay (%d %%)", progress+10));//+10 because seekbar goes from 0 to 90, so translate to 10 to 100
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar)
        {
            SettingsPreferences.FinishAreaMultiplier = (seekBar.getProgress()+10);//+10 because seekbar goes from 0 to 90, so translate to 10 to 100

            SettingsPreferences.savePreferences(SettingsActivity.this);
        }
    };
}
