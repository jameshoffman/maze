package ca.jhoffman.maze.Model.MazeGenerator;

/**
 * Created by jhoffman on 2015-03-04.
 */
public class VerticalNeighborValidator implements INeighborValidator
{
    private int mValue;
    private int mHeight;
    private int mWidth;

    public VerticalNeighborValidator(int value, int width, int height)
    {
        this.mValue = value;
        this.mHeight = height;
        this.mWidth = width;
    }

    public boolean isValid(int cellIndex)
    {
        int verticalIndex = ArrayRepresentationHelper.getArrayRow(cellIndex, mWidth);

        return (verticalIndex + mValue >= 0 && verticalIndex + mValue < mHeight);
    }
}
