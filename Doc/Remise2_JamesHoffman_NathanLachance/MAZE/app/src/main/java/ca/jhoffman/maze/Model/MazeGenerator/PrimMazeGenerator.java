package ca.jhoffman.maze.Model.MazeGenerator;

import java.util.Random;

import ca.jhoffman.maze.Model.FloorCell;
import ca.jhoffman.maze.Model.WallCell;

/**
 * Created by jhoffman on 2015-03-04.
 */
public class PrimMazeGenerator
{
    private static int colsCount;
    private static int rowsCount;

    private static ArrayElement start;
    private static ArrayElement end;

    private static int mCellCount;
    private static Double[][] mEdges;

    private static int[] mNeighborCellOffset;

    private static Integer[][] cellsType;


    public static Integer[][] generate(ArrayElement startElement, ArrayElement endElement, int cols, int rows)
    {
        start = new ArrayElement((startElement.Col+1)/2, (startElement.Row+1)/2);

        end = new ArrayElement((endElement.Col+1)/2, (endElement.Row+1)/2);

        colsCount = (cols+1)/2;
        rowsCount = (rows+1)/2;

        cellsType = new Integer[colsCount][rowsCount];

        for(int i = 0; i < colsCount; i++)
        {
            for(int j = 0; j < rowsCount; j++)
            {
                if(i == 0 || i == (colsCount - 1) || j == 0 || j == (rowsCount-1))
                {
                    cellsType[i][j] = CellType.Wall();
                }
                else
                {
                    Random random = new Random();

                    if(random.nextDouble() > 0.5)
                        cellsType[i][j] = CellType.Floor();
                    else
                        cellsType[i][j] = CellType.Wall();
                }
            }
        }

        cellsType[start.Col][start.Row] = CellType.Start();
        cellsType[end.Col][end.Row] = CellType.End();

        return prim();

        //return cellsType;
    }

    private static Integer[][] prim()
    {
        //Initialization
        mCellCount = colsCount * rowsCount;
        mEdges = new Double[mCellCount][4];

        for (int cellIndex = 0; cellIndex < mCellCount; cellIndex++)
        {
            for (int neighborIndex = 0; neighborIndex < 4; neighborIndex++)
            {
                mEdges[cellIndex][neighborIndex] = null;
            }
        }

        mNeighborCellOffset = new int[]{    -1,         //Left
                                            1,         //Right
                                            -colsCount,    //Top
                                            colsCount};    //Bottom

        initializeRandomEdges();

        Integer[][] labyrinthPath = primEdges();

        return buildCellTypesArray(labyrinthPath);
    }


    private static void initializeRandomEdges()
    {
        Random random = new Random();
        double randomValue = 0;
        INeighborValidator[] neighborCells = { new HorizontalNeighborValidator(-1, colsCount),         //left
                new HorizontalNeighborValidator(1, colsCount),          //right
                new VerticalNeighborValidator(-1, colsCount, rowsCount),  //top
                new VerticalNeighborValidator(1, colsCount, rowsCount)};  //bottom

        //complimentary edge index in the neighbor's edge
        int[] invertedNeighborPositionCell = {  1,      //current has a left neighbor so left neighbor has a right neighbor
                0,      //current has right so left
                3,      //current has top so bottom
                2};     //current has bottom so top

        //Because in the inner for loop, we go through the neighbor cells in the order :
        //left(index 0),
        //right(index 1),
        //top(index 2),
        //bottom(index 3)
        //defined in neighborCells
        //so if a cell has a neighbor, we must also set the neighbor's edge value
        //in the invertedNeighborPositionCell we declare where is the complimentary edge from the neighbor's point of view
        //so :
        //if current cell has a neighbor at => then the neighbor has an edge with the current cell at
        //              left 0               =>              right 1
        //              right 1              =>              left 0
        //              top  2               =>              bottom 3
        //              bottom 3             =>              top 2
        //


        int neighborCellIndex = 0;

        for (int cellIndex = 0; cellIndex < mCellCount; cellIndex++)
        {
            for (int neighborIndex = 0; neighborIndex < neighborCells.length; neighborIndex++)
            {
                if (mEdges[cellIndex][neighborIndex] == null)
                {
                    if (neighborCells[neighborIndex].isValid(cellIndex))
                    {
                        randomValue = random.nextDouble();

                        mEdges[cellIndex][neighborIndex] = randomValue;

                        //initialisation du lien a partir de l'autre noeud impliqué
                        neighborCellIndex = cellIndex + mNeighborCellOffset[neighborIndex];

                        mEdges[neighborCellIndex][invertedNeighborPositionCell[neighborIndex]] = randomValue;
                    }
                }
            }
        }
    }

    private static Integer[][] primEdges()
    {
        Integer[][] primEdges = new Integer[mCellCount][4];
        //offset to get to the neighbor

        int[] invertedNeighborPositionCell = {  1,      //current has a left neighbor so left neighbor has a right neighbor
                0,      //current has right so left
                3,      //current has top so bottom
                2};


        boolean[] usedIndex = new boolean[mCellCount];

        int startIndex = start.Col + (colsCount * start.Row);
        ArrayElement shortestEdgeIndex;//cellIndex, neighborIndex(index range 0 to 3), so the next edge to add in the path
        int neighborCellIndex = 0;

        usedIndex[startIndex] = true;//Start node

        while(hasUnusedIndex(usedIndex))
        {
            shortestEdgeIndex = getShortestEdge(usedIndex);//, usedCellIndexes);

            primEdges[shortestEdgeIndex.Col][shortestEdgeIndex.Row] = CellType.Floor();

            //Update neighbor cell
            neighborCellIndex = shortestEdgeIndex.Col + mNeighborCellOffset[shortestEdgeIndex.Row];
            primEdges[neighborCellIndex][invertedNeighborPositionCell[shortestEdgeIndex.Row]] = CellType.Floor();

            usedIndex[neighborCellIndex] = true;//Update usedIndex to represent that we now use the cell connected to the shortest edge found
        }

        return primEdges;
    }

    private static ArrayElement getShortestEdge(boolean[] usedIndex)//, List<int> usedCellIndexes)
    {
        ArrayElement shortestEdgeIndex = new ArrayElement(0, 0);

        Double minimumWeigth = 1.0;
        for (int idx = 0; idx < usedIndex.length; idx++ )
        {
            if (usedIndex[idx] == true)
            {
                for (int neighborIndex = 0; neighborIndex < 4; neighborIndex++)
                {
                    if (mEdges[idx][neighborIndex] != null)
                    {
                        if (mEdges[idx][neighborIndex] < minimumWeigth
                            && (usedIndex[idx + mNeighborCellOffset[neighborIndex]] == false))
                        {
                            minimumWeigth = mEdges[idx][neighborIndex];
                            shortestEdgeIndex = new ArrayElement(idx, neighborIndex);
                        }
                    }
                }
            }
        }

        return shortestEdgeIndex;
    }

    private static boolean hasUnusedIndex(boolean[] usedIndex)
    {
        for(boolean b : usedIndex)
        {
            if (b == false)
                return true;
        }

        return false;
    }

    private static Integer[][] buildCellTypesArray(Integer[][] labyrinthPath)
    {
        int typedCellsWidth = (colsCount*2)-1;
        int typedCellsHeight = (rowsCount*2)-1;

        Integer[][] typedCells = new Integer[typedCellsWidth][typedCellsHeight];
        for (int i = 0; i < typedCellsWidth; i++)
        {
            for (int j = 0; j < typedCellsHeight; j++)
            {
                typedCells[i][j] = CellType.Wall();
            }
        }

        int col, row;
        int typedCol, typedRow;

        int[] neighborEdgeHorizontal = {    -1,  //Left
                1,  //Right
                0,  //Top
                0}; //Bottom
        int[] neighborEdgeVertical = {    0,  //Left
                0,  //Right
                -1,  //Top
                1}; //Bottom

        for (int cellIndex = 0; cellIndex < mCellCount; cellIndex++)
        {
            col = ArrayRepresentationHelper.getArrayColumn(cellIndex, colsCount);
            row = ArrayRepresentationHelper.getArrayRow(cellIndex, colsCount);

            //typedCol = ArrayRepresentationHelper.getArrayColumn((row+1)*(typedCellsWidth-1) + col*2, typedCellsWidth);
            //typedRow = ArrayRepresentationHelper.getArrayRow((row + 1) * (typedCellsWidth - 1) + col * 2, typedCellsWidth);
            typedCol = col * 2;
            typedRow = row * 2;
            typedCells[typedCol][typedRow] = CellType.Floor();

            for (int neighborIndex = 0; neighborIndex < 4; neighborIndex++)
            {
                if (labyrinthPath[cellIndex][neighborIndex] != null)
                {
                    typedCells[typedCol + neighborEdgeHorizontal[neighborIndex]][typedRow + neighborEdgeVertical[neighborIndex]] = (Integer)labyrinthPath[cellIndex][neighborIndex];

                    mEdges[cellIndex][neighborIndex] = null;
                }
            }
        }

        typedCells[start.Col * 2][start.Row * 2] = CellType.Start();

        typedCells[end.Col * 2][end.Row * 2] = CellType.End();

        return typedCells;
    }
}
