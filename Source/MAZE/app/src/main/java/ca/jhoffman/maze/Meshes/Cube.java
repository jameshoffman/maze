package ca.jhoffman.maze.Meshes;

import java.util.Random;

import static android.opengl.GLES20.GL_TRIANGLES;
import static android.opengl.GLES20.glDrawArrays;
import static android.opengl.GLES20.glUniformMatrix4fv;
import static android.opengl.Matrix.multiplyMM;
import static android.opengl.Matrix.rotateM;
import static android.opengl.Matrix.scaleM;
import static android.opengl.Matrix.setIdentityM;
import static android.opengl.Matrix.translateM;

/**
 * Created by jhoffman on 15-02-13.
 */
public class Cube extends Mesh
{
    public static final int BYTES_PER_FLOAT = 4;

    public static final int POSITION_COMPONENT_COUNT = 3;
    public static final int TEXTURE_COORDINATE_COMPONENT_COUNT = 2;

    public static final int STRIDE = (POSITION_COMPONENT_COUNT + TEXTURE_COORDINATE_COMPONENT_COUNT) * BYTES_PER_FLOAT;

    private static final float mVertices[] = {
            //front,
            -0.5f, -0.5f, 0.5f,    0.0f, 1.0f,
            0.5f, -0.5f, 0.5f,     1.0f, 1.0f,
            -0.5f, 0.5f, 0.5f,     0.0f, 0.0f,
            -0.5f, 0.5f, 0.5f,     0.0f, 0.0f,
            0.5f, -0.5f, 0.5f,     1.0f, 1.0f,
            0.5f, 0.5f,  0.5f,     1.0f, 0.0f,

            //right
            0.5f, -0.5f, 0.5f,     0.0f, 1.0f,
            0.5f, -0.5f, -0.5f,    1.0f, 1.0f,
            0.5f, 0.5f, 0.5f,      0.0f, 0.0f,
            0.5f, 0.5f, 0.5f,      0.0f, 0.0f,
            0.5f, -0.5f, -0.5f,    1.0f, 1.0f,
            0.5f, 0.5f, -0.5f,     1.0f, 0.0f,

            //back
            //Never visible
            /*
            0.5f, -0.5f, -0.5f,     1.0f, 1.0f,
            -0.5f, -0.5f, -0.5f,    0.0f, 1.0f,
            0.5f, 0.5f, -0.5f,      1.0f, 0.0f,
            0.5f, 0.5f, -0.5f,      1.0f, 1.0f,
            -0.5f, -0.5f, -0.5f,    0.0f, 1.0f,
            -0.5f, 0.5f, -0.5f,     0.0f, 0.0f,
            */

            //left
            -0.5f, -0.5f, -0.5f,    0.0f, 1.0f,
            -0.5f, -0.5f, 0.5f,     1.0f, 1.0f,
            -0.5f, 0.5f, -0.5f,     0.0f, 0.0f,
            -0.5f, 0.5f, -0.5f,     0.0f, 0.0f,
            -0.5f, -0.5f, 0.5f,     1.0f, 1.0f,
            -0.5f, 0.5f, 0.5f,      1.0f, 0.0f,


            //bottom

            -0.5f, -0.5f, -0.5f,    1.0f, 0.0f,
            0.5f, -0.5f, -0.5f,     1.0f, 1.0f,
            -0.5f, -0.5f, 0.5f,     0.0f, 0.0f,
            -0.5f, -0.5f, 0.5f,     0.0f, 0.0f,
            0.5f, -0.5f, -0.5f,     1.0f, 1.0f,
            0.5f, -0.5f, 0.5f,      0.0f, 1.0f,



            //top
            -0.5f, 0.5f, 0.5f,     0.0f, 1.0f,
            0.5f, 0.5f, 0.5f,      1.0f, 1.0f,
            -0.5f, 0.5f, -0.5f,    0.0f, 0.0f,
            -0.5f, 0.5f, -0.5f,    0.0f, 0.0f,
            0.5f, 0.5f, 0.5f,      1.0f, 1.0f,
            0.5f, 0.5f, -0.5f,     1.0f, 0.0f
    };

    private float[] mModelMatrix;

    private float scaleX;
    private float scaleY;
    private float scaleZ;

    private float x;
    private float y;
    private float z;

    //**********************************************************************************************
    // Static getters
    //**********************************************************************************************

    public static int getVerticesFloatSize()
    {
        return mVertices.length * BYTES_PER_FLOAT;
    }

    public static float[] getVertices() {
        return mVertices;
    }

    public static int getVerticesCount() {
        return mVertices.length/(POSITION_COMPONENT_COUNT + TEXTURE_COORDINATE_COMPONENT_COUNT);
    }

    //**********************************************************************************************
    // Constructor
    //**********************************************************************************************
    public Cube()
    {
        mModelMatrix = new float[16];
        setIdentityM(mModelMatrix, 0);
    }

    //**********************************************************************************************
    // Methods
    //**********************************************************************************************

    //draw
    public void draw(float[] projectionViewMatrix, int projectionPosition, int modelPosition)
    {
        glUniformMatrix4fv(projectionPosition, 1, false, projectionViewMatrix, 0);

        glUniformMatrix4fv(modelPosition, 1, false, mModelMatrix, 0);

        glDrawArrays(GL_TRIANGLES, 0, getVerticesCount());
    }

    @Override
    public void setScale(float scaleX, float scaleY, float scaleZ)
    {
        this.scaleX = scaleX;
        this.scaleY = scaleY;
        this.scaleZ = scaleZ;

        setModelMatrix();
    }

    @Override
    public void setPosition(float x, float y, float z)
    {
        this.x = x;
        this.y = y;
        this.z = z;

        setModelMatrix();
    }

    public void setPositionZ(float z)
    {
        this.z = z;

        setModelMatrix();
    }

    private void setModelMatrix()
    {
        setIdentityM(mModelMatrix, 0);

        translateM(mModelMatrix, 0, x, y, z);

        scaleM(mModelMatrix, 0, scaleX, scaleY, scaleZ);
    }
}
