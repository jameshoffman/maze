uniform mat4 uniform_matrix;
uniform mat4 uniform_model_matrix;

attribute vec4 attribute_position;

attribute vec4 attribute_color;
varying vec4 varying_color;

uniform float uniform_point_size;

void main()
{
    mat4 mvp = (uniform_matrix * uniform_model_matrix);
    gl_Position =  mvp * attribute_position;

    varying_color = attribute_color;
}