package ca.jhoffman.maze.Model.MazeGenerator;

/**
 * Created by jhoffman on 2015-03-04.
 */
public class ArrayElement
{
    public int Col;
    public int Row;

    public ArrayElement(int col, int row)
    {
        this.Col = col;
        this.Row = row;
    }
}
