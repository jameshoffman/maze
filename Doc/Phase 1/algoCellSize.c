#include <math.h>
#include <stdlib.h>
#include <stdio.h>

#define MIN(x,y)    (x)<(y)?(x):(y) 
#define ABS(val)    (val)<(0)?(-1*val):(val) 

double diff = 2.0;
void sizeByArea(double x, double y, double cols, double rows)
{
	 //double n=cols*rows;//values here
	
	 //double parentArea = x * y;
	
	 //double squareArea = parentArea / ( double)n;
	
	//sqrt(squareArea)
 //double size = sqrt(squareArea);

double w = x/cols;
double h = y/rows;

double tmpDiff = fabs(w - h);

if(tmpDiff < diff)
{
	diff = tmpDiff;
	
	printf("cols, rows = %.20lf, %.20lf\n", cols, rows);
	printf("diff = %lf\n", diff);
	printf("size = %.20lf, %.20lf\n", x/cols, y/rows);
	printf("result = %.20lf, %.20lf\n\n\n\n", x/(x/cols), y/(y/rows));
}
}

int main()
{
	double w = 1920;
	double h = 1080;
	double x=((w/h)*2.0);
	double y=2.0;
	double aspectRatio = x/y;
	
	int c = 0;
	for(int i = ceil(10*(aspectRatio)); i < ceil(15*(aspectRatio)); i++)
	{
		for(int j = 10; j < 15; j++)
		{
			c++;
			sizeByArea(x, y, i, j);
		}
	}
	
	printf("%i", c);
	
	return 0;
}