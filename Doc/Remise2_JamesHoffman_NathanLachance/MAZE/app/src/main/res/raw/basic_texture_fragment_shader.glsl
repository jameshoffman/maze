precision mediump float;

uniform sampler2D uniform_texture_unit;

varying vec2 varying_texture_coordinates;

void main()
{
    gl_FragColor = texture2D(uniform_texture_unit, varying_texture_coordinates);
}