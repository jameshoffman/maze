package ca.jhoffman.maze.GLUtils;

import android.opengl.GLES20;

/**
 * Created by jhoffman on 15-02-06.
 */
public enum GLShaderType
{
    Vertex20,
    Fragment20;

    public int getGLShaderType()
    {
        switch(this)
        {
            case Vertex20:
                return GLES20.GL_VERTEX_SHADER;
            case Fragment20:
                return GLES20.GL_FRAGMENT_SHADER;
            default:
                throw new EnumConstantNotPresentException(GLShaderType.class, this.name());
        }
    }
}
